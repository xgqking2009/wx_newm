// pages/user/infos.js
//用户信息
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var testData = require('../../utils/testData.js')
//获取应用实例
const app = getApp()
Page({ 
  /**
   * 页面的初始数据
   * 课程  1-》n 打卡路线 1 -》n 打卡任务
   * course     taskLine        task
   */  
  data: {
    userInfo:null,    //用户信息
    sumTotalScore:0,     //用户积分
    phoneNumber:'',//用户手机号码
    courseInfoList: null,//上课时间的分组数据集合
    btns:{}, 
    pscourse:null,
    ccourse:null,
    acourse:null,
    courselen:0,
    mapidx:['A', 'B', 'C', 'D', 'E', 'F','G','H','I']
  },   
  tapName: function (x,y) {
    var x = x/global.vw,y = y/global.vh;
    var btns = this.data.btns;
    for(var k in btns){
      var bs = btns[k].bs;
      if(x>=bs.x&&x<bs.x+bs.w&&y>=bs.y&&y<bs.y+bs.h){
        btns[k].onClick();
      }  
    } 
  }, 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.processData();
    this.setData({
      pscourse: this.coursedata.pscourse,
      ccourse:this.coursedata.ccourse,
      acourse: this.coursedata.acourse,
      psl: this.coursedata.acourse.length
    });
  },
  onBack: function () {
    util.reLaunch('../user/infos',app);
    // util.Back(1);
  },  
  processData:function(){
    var gd = app.globalData.coursePlanData;
    var pcourse = [], scourse = [], ccourse = [], pscourse = [],acourse = [];
    for (var k = 0; k < gd.length; k++) {
      var nd = gd[k], tl = nd.taskLine; var planBeginTime = nd.planBeginTime;
      for (var j = 0; j < tl.length; j++) {
        var t = tl[j];
        t.planBeginTime = planBeginTime;
        console.log("Xgq item.recordStatus:" + t.recordStatus + " " + t.coursePlannedRouteId);
        // t.recordStatus = 'PLANNING';
        // t.coursePlannedRouteId = null;
        // COMPLETED
        if (t.recordStatus == 'PLANNING') { pcourse.push(t); pscourse.push(t); acourse.push(t); }
        else if (t.recordStatus == 'STARTED') { scourse.push(t); pscourse.push(t); acourse.push(t);}
        else { ccourse.push(t); acourse.push(t);}
      }
    }
    // acourse = acourse.slice(0,1);
    this.coursedata = {
      pscourse: pscourse,
      ccourse:ccourse,
      acourse: acourse,
      psl: acourse.length
    }
  },

  touchCStart:function(e){
    this.touchx = e.touches[0].x;
    this.touchy = e.touches[0].y;
  },
  touchCMove:function(){
  },
  touchCEnd:function(e){
    var x = e.changedTouches[0].x, y = e.changedTouches[0].y;
    var dx = x - this.touchx,dy = y - this.touchy;
    if(dx*dx+dy*dy<10000){
      this.tapName(e.changedTouches[0].x, e.changedTouches[0].y);
    }
  },
  doFrame: function(){
    var self = this;
    app.globalData.appState = 'route';
    if (app.globalData.appState == 'main') {
      var cm = global.drawImage("../img/BG_Tree_001.png", -2, 0);
      var c1 = global.drawImage("../img/BG_Tree_002.png", 0.008, -0.032);
      var b1 = global.drawImage("../img/Botton_007_Normal.png", 0.6, 0.2);
      var b2 = global.drawImage("../img/Botton_008_Normal.png", 0.66, 0.38);
      if (!self.data.btns.b2) {
        self.data.btns.b2 = {
          bs: b2, onClick: function () {
            app.globalData.appState = 'route';
          }
        }
      }
      var r1 = global.drawImage("../img/Role_001.png", 0.17, 0.6);
      var rb1 = global.drawImage("../img/Bubble_001.png", 0.45, 0.62);
    }
    else if (app.globalData.appState == 'route') 
    {
      var k = 0; var gd = app.globalData.coursePlanData;
      if (gd.length == 0) {
        global.drawImage("../img/BG_Sky_001.png", -2, 0);
        var sy = 0.1;
        var bs = global.drawImage("../img/Botton_Return_Normal.png", 0.02, 0.06);
        if (!self.data.btns.bback) {
          self.data.btns.bback = {
            bs: bs, onClick: function () {
              app.globalData.appState = 'main';
            }
          } 
        }
        global.drawImage("../img/Panel_013.png", 0.06, 0.06 + sy);
        global.circleClip(0.0956, 0.0788 + sy, 0.078);
        global.drawImage(self.data.userInfo.avatarUrl, 0.08, 0.08 + sy, 0.24, 0.24 / 1.78);
        global.restore();
        global.drawText(self.data.userInfo.nickName, 0.33, 0.14 + sy, 14, "#FFFFFF");
        global.drawImage("../img/Role_001.png", 0.35, 0.3 + sy);
        global.drawImage("../img/Bubble_010.png", 0.6, 0.2 + sy);
      }
      else {
        var cm = global.drawImage("../img/BG_river_001.png", -3, 0);
        var cs = global.drawImage("../img/Star_001.png", 0.22, 0.07);
        var ox, oy, c = 0, rx, ry, tx, ty, bx, by;
        var offs = [0.4, 0.12, 0.11, 0.3, 0.3, 0.48, 0.2, 0.63, 0.1, 0.8];
        var offl = [0, 0, 0.4, 0, 0.015, 0.03, 0.03, 0.05, 0.03, 0.05, 0.058, 0.02, 0.2, 0.07];
        var offr = [0, 0, -0.08, 0, 0.09, 0.03, 0.33, 0.045, 0.3, 0.045, 0.058, 0.02, 0.1, 0.07];
        var offc = [0, 0, 0.058, 0.03, 0.03, 0.25, -0.005, 0.46, 0.02, 0.445, 0.03];
        var pcourse = [], scourse = [], ccourse = [], pscourse = [];
        for (var k = 0; k < gd.length; k++) {
          var nd = gd[k], tl = nd.taskLine; var planBeginTime = nd.planBeginTime;
          for (var j = 0; j < tl.length; j++) {
            var t = tl[j];
            t.planBeginTime = planBeginTime;
            if (t.recordStatus == 'PLANNING') { pcourse.push(t); pscourse.push(t); }
            else if (t.recordStatus == 'STARTED') { scourse.push(t); pscourse.push(t); }
            else ccourse.push(t);
          }
        }
        for (var k = 0; k < pscourse.length; k++) {
          var t = pscourse[k];
          // for(var tkey in t){
          //   console.log("Xgq tkey:"+tkey+" v:"+t[tkey]);
          // }
          var tcn = t.coursePlannedRouteName;
          tcn = tcn ? (tcn.length > 4 ? tcn.slice(0, 4) : tcn) : "";
          var ix = offs[c * 2], iy = offs[c * 2 + 1]; c++;
          var off = (c % 2 == 1) ? offl : offr;
          var ox = off[0] + ix, oy = off[1] + iy, rx = off[2] + ix, ry = off[3] + iy, tx = off[4] + ix, ty = off[5] + iy, bx = off[6] + ix, by = off[7] + iy, bsx = off[8] + ix, bsy = off[9] + iy, btx = off[10], bty = off[11], timex = off[12] + ix, timey = off[13] + iy;
          var cs = t.courseApplyName.length > 10 ? t.courseApplyName.slice(0, 9) + "..." : t.courseApplyName;
          var cindex = t.coursePlannedRouteIndex == null ? -1 : t.coursePlannedRouteIndex;
          if (t.recordStatus == 'PLANNING') {
            var c1 = global.drawImage("../img/Panel_010.png", ox, oy);
            var cbr;
            if (cindex == -1)
              ;//cbr = global.drawImage("../img/Botton_None.png", rx, ry);
            else {
              cbr = global.drawImage("../img/Botton_" + global.mapidx[cindex] + "_Normal.png", rx, ry);
              var rkey = 'cbr_' + k;
              self.data.btns[rkey] = {
                bs: cbr, sid: t.studentCoursePlannedRouteRecordId,
                key: ckey, cid: t.courseApplyId, name: t.coursePlannedRouteName, crid: t.coursePlannedRouteId
              };
              self.data.btns[rkey].onClick = (function () {
                var tbn = self.data.btns[this];
                app.globalData.studentCoursePlannedRouteRecordId = tbn.sid;
                console.log("Xgq 1 tbn.cid:"+tbn.cid);
                app.globalData.courseApplyId = tbn.cid;
                app.globalData.routeName = tbn.name;
                app.globalData.coursePlannedRouteId = tbn.crid;
                app.globalData.isSelectPage = false;
                app.globalData.isScanTask = true;
                util.navigateTo('../task/selsec', app);
              }).bind(rkey);
            }
            global.drawText(cs, tx, ty, 14, "#FFFFFF");
            global.drawText(t.planBeginTime, timex, timey, 14, "#FFFFFF");
            var c1d;
            if (t.coursePlannedRouteId == null) {
              c1d = global.drawImage("../img/Botton_011_Normal.png", bx, by); var ckey = "course_1_" + c;
            } else {
              var c1 = global.drawImage("../img/Panel_010.png", ox, oy);
              global.drawImage("../img/Botton_" + global.mapidx[cindex] + "_Normal.png", rx, ry);
              global.drawText(cs, tx, ty, 14, "#FFFFFF");
              c1d = global.drawImage("../img/Botton_010_Normal.png", bsx, bsy);
              global.drawText(tcn, bsx + btx, bsy + bty, 9, "#FFE3A2");
              global.drawText(t.planBeginTime, timex, timey, 14, "#FFFFFF");
            }
            self.data.btns[ckey] = {
              bs: c1d, sid: t.studentCoursePlannedRouteRecordId,
              key: ckey, cid: t.courseApplyId, name: t.coursePlannedRouteName, crid: t.coursePlannedRouteId
            };
            self.data.btns[ckey].onClick = (function () {
              var tbn = self.data.btns[this];
              app.globalData.studentCoursePlannedRouteRecordId = tbn.sid;
              console.log("Xgq 2 tbn.cid:" + tbn.cid);
              app.globalData.courseApplyId = tbn.cid;
              app.globalData.routeName = tbn.name;
              app.globalData.coursePlannedRouteId = tbn.crid;
              app.globalData.isSelectPage = true;
              app.globalData.isScanTask = false;
              util.navigateTo('../task/selsec', app);
            }).bind(ckey);
          }
          else if (t.recordStatus == 'STARTED') {
            var c1 = global.drawImage("../img/Panel_010.png", ox, oy);
            global.drawImage("../img/Botton_" + global.mapidx[cindex] + "_Normal.png", rx, ry);
            global.drawText(cs, tx, ty, 14, "#FFFFFF");
            global.drawText(tcn, bsx + btx - 0.05, bsy + bty, 14, "#FFE3A2");
            global.drawText(t.planBeginTime, timex, timey, 14, "#FFFFFF");
          }
        }
        for (var k = 0; k < ccourse.length; k++) {
          var t = ccourse[k];
          var tcn = t.coursePlannedRouteName;
          tcn = tcn ? (tcn.length > 4 ? tcn.slice(0, 4) : tcn) : "";
          var cs = t.courseApplyName.length > 10 ? t.courseApplyName.slice(0, 9) + "..." : t.courseApplyName;
          var ix = offs[c * 2], iy = offs[c * 2 + 1]; c++;
          var ox = offc[0] + ix, oy = offc[1] + iy, clx = offc[2], cly = offc[3], clp = offc[4];
          var cgx = offc[5], cgy = offc[6], cctx = offc[7], ccty = offc[8], crx = offc[9], cry = offc[10];
          var c1 = global.drawImage("../img/Panel_011.png", ox, oy);
          var csb = cs.slice(0, 5), csa = cs.slice(5, cs.length);
          global.drawText(csb, ox + clx, oy + cly, 13, "#844F16");
          global.drawText(csa, ox + clx, oy + cly + clp, 13, "#844F16");
          global.drawImage("../img/Botton_013_Normal.png", ox + cgx, oy + cgy);
          global.drawText(tcn, ox + cctx, oy + ccty, 10, "#844F16");
          global.drawImage("../img/Botton_012_Normal.png", ox + crx, oy + cry);
        }
      }
    }
    wx.drawCanvas({ canvasId: 1, actions: global.ctx.getActions() });
  },
  onReady: function () {
    var self = this;
    app.globalData.internal = setInterval(this.doFrame,16.6);
  },
  updatBtnInfo(e){
    var that = this;
    var studentCoursePlannedRouteRecordId = e.target.dataset.sid;
    var courseApplyId = e.target.dataset.cid;
    var coursePlannedRouteId = e.target.dataset.pid;
    var routeName = e.target.dataset.name;
    console.log("Xgq studentCoursePlannedRouteRecordId:" + studentCoursePlannedRouteRecordId);
    app.globalData.studentCoursePlannedRouteRecordId = studentCoursePlannedRouteRecordId;
    app.globalData.courseApplyId = courseApplyId;
    app.globalData.routeName = routeName;
    app.globalData.coursePlannedRouteId = coursePlannedRouteId;
  },
  /** 
   * 点击修改路线
   */
  onChangeline:function(e)
  {
    var that = this;
    that.updatBtnInfo(e);
    app.globalData.isSelectPage = false;
    app.globalData.isScanTask = false;
    util.navigateTo('../task/selsec',app);
  },
  /**
   * 选择路线
   */
  onSelectLine:function(e)
  {
    var that = this;
    that.updatBtnInfo(e);
    app.globalData.isSelectPage = true;
    app.globalData.isScanTask = false;
    util.navigateTo('../task/selsec', app);
  },
  /**
   * 计划状态下 点击 打卡
   * 要先通知服务器确认 然后进入
   */
  onSureAnEnterLine:function(e){
    var that = this;
    that.updatBtnInfo(e);
    if (app.globalData.enterTaskNeedConfirm)
    {
      tool.showModal('确定进入 ' + app.globalData.routeName + ' 路线，一旦确定则不可修改', null, that.onEnterConfirm, that.onEnterCancel);
    }
    else
    {
      that.onEnterConfirm();
    }
  },
  /**
  * 确认选择
  */
  onEnterConfirm() {
    var that = this;
    if (tool.isLoading(app)) {
      return;
    }
    tool.showLoading(app, that);
    var url = app.data.apiUrl + "studentCoursePlannedRouteRecords/" + app.globalData.studentCoursePlannedRouteRecordId + "/startedStudentCoursePlannedRouteRecord";
    var header = util.getTokenHeadr(app);
    util.request(url, null, that.onEnterCallBack, null, null, null, header);
  },
  onEnterCancel(){

  },
  onEnterCallBack:function()
  {
    var that = this;
    tool.hideLoading(app,that);
    util.navigateTo('../task/scantask', app);
  },
  /**
   * 查看排行榜
   */ 
  onShowSort:function(e)
  {
    var that = this;
    var customerUserOrdersCourseId = e.target.dataset.oid;
    app.globalData.courseApplyName = e.target.dataset.cname;
    app.globalData.customerUserOrdersCourseId = customerUserOrdersCourseId;
    console.log("Xgq app.globalData.customerUserOrdersCourseId:" + app.globalData.customerUserOrdersCourseId);
    util.navigateTo('../sort/sort', app);
  },
  /**
   * 打卡 选择路线 按钮点击
   */
  onTaskBtn:function(e)
  {
    var that = this;
    that.updatBtnInfo(e);
    app.globalData.isScanTask = true;
    util.navigateTo('../task/selsec',app);
  },
  /**
   * 打卡APP下载页面
   */
  onOpenApp:function(e)
  {
    util.navigateTo('../app/down',app);
  },
  /**
   * 打开用户管理界面
   */
  onOpenUser:function(e)
  {
    util.navigateTo('../user/manager', app);
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getUserInfoFromServer();
    // var data = testData.plannedData();
    // var taskData = that.parsInfo(data);
    // that.setData({
    //   courseInfoList: taskData
    // });
  },
  /**
   * 从服务器获取首页数据
   */
  getUserInfoFromServer:function(callBackFunc)
  {
    console.log("Xgq getUserInfoFromServer.....");
    var that = this;
    if(tool.isLoading(app))
    {
      if (null != callBackFunc)
      {
        callBackFunc();
      }
      return;
    }
    tool.showLoading(app,that);
    var url = app.data.apiUrl + '/studentCoursePlannedRouteRecords/miniAppHomePage';
    var data = { phoneNumber: app.globalData.phoneNumber };
    var header = util.getTokenHeadr(app);
    util.request(url, data, that.getUserInfoCallBack, callBackFunc,null,'get',header);
  },
  /**
   * 获取用户信息回调
   */
  getUserInfoCallBack:function(data,param)
  {
    var that = this;  
    tool.hideLoading(app,that);
    var sumStudentCoursePlannedRouteRecordBean = data['responseBody'];
    console.log("<<<<<<<<<< 222 A 首页获取课程数据：\n\t\t" + JSON.stringify(taskData));
    var taskData = that.parsInfo(sumStudentCoursePlannedRouteRecordBean.groupByPlanBeginTimePlannedRouteRecords);
    app.globalData.coursePlanData = taskData; 
    
    console.log("<<<<<<<<<< 222 B 首页获取课程数据：\n\t\t" + JSON.stringify(taskData));
    that.processData();
    that.setData({
      userInfo : app.globalData.userInfo, 
      phoneNumber: app.globalData.phoneNumber,
      sumTotalScore: sumStudentCoursePlannedRouteRecordBean.sumTotalScore,
      courseInfoList: taskData,
      pscourse: that.coursedata.pscourse,
      ccourse:that.coursedata.ccourse,
      acourse: that.coursedata.acourse,
      psl: that.coursedata.acourse.length
    });
    if(null != param)
    {
      param();
    }
  },

  /**
   * 解析数据
   */
  parsInfo:function(data)
  {
    if(data == null)
    {
      return;
    }
    var that = this;
    var tmpData = [];
    for (var i = 0; i < data.length; i++) {
      ////上课时间的分组数据
      var dataItem = data[i];
      var planneGroupObj = {};
      planneGroupObj.planBeginTime = tool.formatTimeYMD(dataItem.planBeginTime,'Y/M/D');
      var groupByStatusPlannedRouteRecords = dataItem.groupByStatusPlannedRouteRecords;
      planneGroupObj.taskLine = [];
      that.parsGroup(planneGroupObj.taskLine, groupByStatusPlannedRouteRecords);
      tmpData.push(planneGroupObj);
    }
    return tmpData;
  },
  parsGroup: function (taskLine,group)
  {
    var planningList = null;
    var startedList = null;
    var completedList = null;
    var that = this;

    for (var i = 0; i < group.length; i++) {
      var item = group[i];
      if (item.recordStatus == "PLANNING")
      {
        planningList = item.studentCoursePlannedRouteRecords;
      }
      else if (item.recordStatus == "STARTED")
      {
        startedList = item.studentCoursePlannedRouteRecords;
      }
      else if (item.recordStatus == "COMPLETED")
      {
        completedList = item.studentCoursePlannedRouteRecords;
      }
    }
    that.parsTask(taskLine, planningList);
    that.parsTask(taskLine, startedList);
    that.parsTask(taskLine, completedList);
  },
  parsTask:function(taskLine,data)
  {
    if(null == data)
    {
      return;
    }
    for (var i = 0; i < data.length; i++) {
      // var itemObj = {};
      var dataItem = data[i];
      var itemObj = global.CLONE(dataItem);
      //   itemObj.studentCoursePlannedRouteRecordId = dataItem.studentCoursePlannedRouteRecordId;//学生课程记录id
      //   itemObj.phoneNumber = dataItem.phoneNumber; //属于那个手机号的
      //   itemObj.fullName = dataItem.fullName; //名字
      //   itemObj.avatarUrl = dataItem.avatarUrl; //头像
      //   itemObj.courseApplyId = dataItem.courseApplyId; //申请课程id
      //   itemObj.courseApplyName = dataItem.courseApplyName;  //申请课程名字
      //   itemObj.customerUserOrdersCourseId = dataItem.customerUserOrdersCourseId; //第二级订单id(课程订单id)
      //   itemObj.coursePlannedRouteId = dataItem.coursePlannedRouteId; //路线id
      //   itemObj.coursePlannedRouteName = dataItem.coursePlannedRouteName; //路线的名字
      //   itemObj.coursePlannedRoute = dataItem.coursePlannedRoute; //路线
      //   itemObj.planBeginTime = dataItem.planBeginTime;          //计划上课时间
      //   itemObj.coursePlannedRouteIndex = dataItem.coursePlannedRouteIndex;
      //   itemObj.recordStatus = dataItem.recordStatus;  //该记录的状态
      //   itemObj.submitTime = dataItem.submitTime; //完成路线时间
      //   itemObj.totalScore = dataItem.totalScore; //总的得分
      //   itemObj.rankRecord = dataItem.rankRecord; //排名情况
      // itemObj.coursePlannedRouteName = dataItem.coursePlannedRouteName;
      itemObj.selectLineName = dataItem.coursePlannedRouteName;//dataItem.coursePlannedRouteId + "线路(" + dataItem.coursePlannedRouteName+")";
      taskLine.push(itemObj);
    }
   
  },
  

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  },

  // onPullDownRefresh:function()
  // {
  //   // var that = this;
  //   // if(tool.isLoading(app))
  //   // {
  //   //   util.sotpPullDownRefresh();
  //   //   return;
  //   // }
  //   // that.getUserInfoFromServer(util.sotpPullDownRefresh);
  // },
})