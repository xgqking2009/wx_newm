//index.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
//获取应用实例
const app = getApp()
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    btntop:0, 
    iv:null,
    encryptedData:null,

    wxappName:null,
    phoneNumber:null,

    isLoading:false,
  }, 
  onStart: function (event) {
    
    this.starX = event.touches[0].clientX / global.vw;
  },
  onMove: function (event) {
    this.starDX = (event.touches[0].clientX / global.vw - this.starX)*3;
  },
  onEnd: function (event) {
    this.listStar.x = this.listStar.x + this.starDX;
    this.starDX = 0;
  }, 
  tapName: function (event) {
    var x = event.detail.x / global.vw, y = event.detail.y / global.vh;
    var btns = this.data.btns;
    for (var k in btns) {
      var bs = btns[k].bs;
      if (x >= bs.x && x < bs.x + bs.w && y >= bs.y && y < bs.y + bs.h) {
        btns[k].onClick();
        if(btns[k].onEffect) btns[k].onEffect();
      }
    } 
  },
  onLoad: function () {
    var listStar = {};
    listStar.lx = listStar.x = 0.15;
    listStar.w = 0.7;
    this.starDX = 0;
    this.listStar = listStar;
    this.btns = [];
    this.setData({
      btntop1: 40 + 14 * global.s,
      btntop2: 40 + 28 * global.s
    })
    console.log("Xgq vw:"+global.vw);
  },

  

  onReady: function () {
    var context = wx.createContext();
    var self = this;
    this.ID = 0;
    app.globalData.interval = setInterval(function doFrame(){
      var dpg = 0;
      if(dpg==0){
        var cm = global.drawImage( "../img/Panel_001.png", -1, 0.4);
        var s = global.vw / global.vh;
        global.canDrawImage = false;
        var b1 = global.drawImage("../img/Botton_001_Normal.png", -1, 0.14 * global.s, cm);
        var b2 = global.drawImage("../img/Botton_002_Normal.png", -1, 0.28 * global.s, cm);
        global.canDrawImage = true;
        // global.drawTextInRect("中了手机砥砺奋进卡德加法拉第接口发杰拉德积分卡圣诞节福利卡就是对方立即打上了飞机啊乐山大佛大家覅京东方科技是打开房间爱对了师傅",
        //   0.1,0.2,0.6,0.4,5,12,'#ff0000');
        var allbtns = {'b1':b1,'b2':b2};
        global.addBtns(allbtns);
        global.loopBtns();
      }else{
        // global.drawImage("../img/Role_001.png", 0.1, 0.22);
        // var bn = global.drawImage("../img/Botton_018_Normal.png", 0.36, 0.6);
      }  
      wx.drawCanvas({
        canvasId: 1,
        actions: global.ctx.getActions()
      });
    }, 16.6);
  },
  /**
   * 获取手机号码
   */
  onWxLogin:function(){
    console.log("onWxLogin.......");
    global.btns['b1'].onEffect();
  },
  getPhoneNumber : function(e)
  {
    clearInterval(this.itr); 
    var that = this;
    setTimeout(
    function(){
      if (e.detail.errMsg != null && e.detail.errMsg == "getPhoneNumber:fail user deny") {
        return;
      }
      if (tool.isLoading(app)) {
        return;
      }
      tool.showLoading(app, that);
      that.data.iv = e.detail.iv;
      that.data.encryptedData = e.detail.encryptedData;
      that.login(that.doGetPhoneNum);
    },1000);
  },
  /**
   * 登录
   */
  login:function(callBackFunc)
  {
    setTimeout(function(){
      clearInterval(app.globalData.interval);
    },200);
    var that = this;
    if(app.globalData.sessionKey != null)
    {
      wx.checkSession({
        success: function () {
        //session 未过期，并且在本生命周期一直有效
        callBackFunc();
      },
      fail: function () {
        //登录态过期
        that.doLogin(callBackFunc);
        }
      });
    }
    else
    {
      that.doLogin(callBackFunc);
    }
  },

  /**
   * 微信登陆
   */
  doLogin:function(callBackFunc)
  {
    var that = this;
    app.globalData.appState = "main";
    wx.login({
      success: res => {
        console.log("cod: " + res.code);
        var url = app.data.apiUrl + "wxMiniApp/login";
        util.request(url, { code: res.code }, that.getsessionkeysCallBack, callBackFunc,null,'get')
      }
    });
  },
  
  /**
   * 服务器返回了openid，session_key
   */
  getsessionkeysCallBack: function (data, callBackFunc)
  {
    var that = this;
    var responseBody = data['responseBody'];
    app.globalData.sessionKey = responseBody['sessionKey'];
    app.globalData.openID = responseBody['openid'];
    app.onSaveSeesion(responseBody);
    console.log("session_key: " + app.globalData.sessionKey);;
    callBackFunc();
  },
  /**
   * 执行获取手机号码
   */
  doGetPhoneNum:function()
  {
    var that = this;
    var url = app.data.apiUrl +"wxMiniApp/oneKeyAuthorization";
    var data = {
      sessionKey: app.globalData.sessionKey,
      openid:app.globalData.openID,
      encryptedData: that.data.encryptedData,
      iv: that.data.iv
    };
    
    util.request(url, data, that.getPhoneNumerCallBack,null,null,'get');
  },
  
  /**
   * 服务端返回手机号码
   */
  getPhoneNumerCallBack:function(data)
  {
    var that = this;
    var responseBody = data['responseBody'];
    app.globalData.phoneNumber = responseBody['phoneNoInfo']['phoneNumber'];
    app.globalData.purePhoneNumber = responseBody['phoneNoInfo']['purePhoneNumber'];
    app.globalData.countryCode = responseBody['phoneNoInfo']['countryCode'];

    tool.hideLoading(app,that);
    if (responseBody['customerUser'] == null)
    {
      util.navigateTo('../login/wxlogin',app);
    }
    else
    {
      var customerUserBean = responseBody['customerUser'];
      app.globalData.customerUserBean = customerUserBean;
      app.globalData.token = customerUserBean.token.newToken;

      //app.globalData.userInfo = responseBody['userInfo'];

      //注册成功
      util.redirectTo('../user/infos', app);
    }
  },
  
  /**
   * 获取用户信息
   */
  // getUserInfo: function(e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     hasUserInfo: true
  //   })
  // },

  /**
   * 打开手机号码登陆页面
   */
  onPhoneLogin:function(e){
    var that = this;
    global.btns['b2'].onEffect();
    if(tool.isLoading(app))
    {
      return;
    }
    tool.showLoading(app,that);
    that.login(that.doOpenPhoneLogin);
    
  },
  /**
   * 打开手机注册页面
   */
  doOpenPhoneLogin:function()
  {
    var that = this;
    tool.hideLoading(app,that);
    util.navigateTo('../login/phlogin', app);
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  }

})
