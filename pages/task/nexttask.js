// pages/task/nexttask.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lastTaskCount: 0,//当前路线还有多少节点没有打卡的数量
    taskScore: 0,//答题得分
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.updataInfo();
  },
  tapName: function (event) {
    var x = event.detail.x / global.vw, y = event.detail.y / global.vh;
    var btns = this.data.btns;
    for (var k in btns) {
      var bs = btns[k].bs;
      if (x >= bs.x && x < bs.x + bs.w && y >= bs.y && y < bs.y + bs.h) {
        btns[k].onClick();
      }
    }
  }, 
  onReady: function () {
    var self = this;
    app.globalData.internal = setInterval(function doFrame() {
      global.drawImage("../img/BG_Sky_001.png", -2, 0);
      global.drawImage("../img/Star_006.png", 0.17, 0.18);
      global.drawImage("../img/Panel_004.png", 0.22, 0.3);
      global.drawText("" + taskScore, 0.45, 0.422, 24, '#FF7800');
      var bn = global.drawImage("../img/Botton_019_Normal.png", 0.35, 0.45);
      // self.data.btns["bn"] = {
      //   bs: bn, onClick: (function () {
      //     self.onSumbit();
      //   }).bind(k)
      // }
      wx.drawCanvas({ canvasId: 1, actions: global.ctx.getActions() });
    }, 0.0167);
  },

  /**
   * 更新数据
   */
  updataInfo:function(){
    var that = this;
    that.setData({
      lastTaskCount : app.globalData.lastTaskCount,
      taskScore : app.globalData.taskScore
    });
  },
  /**
   * 进入下一个打卡任务
   */
  onEnterNext:function(){
    var that = this;
    if(that.data.lastTaskCount>0){ 
      util.reLaunch("../task/scantask",app);
    }
    else
    {
      util.reLaunch("../user/course",app);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  }
})