// pages/task/task.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var testData = require('../../utils/testData.js')
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrolls: [{ num: 0 }, { num: 1 }, { num: 2 }],
    selectOptinIndex: null, //当前选中题目的索引
    fraction:null, //正确答案的得分
    correctAnswerIndex:null,//正确答案的索引
    studentPlannedRouteTaskNodeRecordBean: null,//当前打卡的任务节点
    isLoading:false,
    btns:[],
  },
  // tapName: function (event) {
  //   var x = event.detail.x / global.vw, y = event.detail.y / global.vh;
  //   var btns = this.data.btns;
  //   for (var k in btns) {
  //     var bs = btns[k].bs;
  //     if (x >= bs.x && x < bs.x + bs.w && y >= bs.y && y < bs.y + bs.h) {
  //       btns[k].onClick();
  //     }
  //   }
  // },
  // touchCStart: function (e) {
  //   this.touchx = e.touches[0].x;
  //   this.touchy = e.touches[0].y;
  // },
  // touchCMove: function () {
  // },
  // touchCEnd: function (e) {
  //   var x = e.changedTouches[0].x, y = e.changedTouches[0].y;
  //   var dx = x - this.touchx, dy = y - this.touchy;
  //   if (dx * dx + dy * dy < 10000) {
  //     this.tapName(e.changedTouches[0].x, e.changedTouches[0].y);
  //   }
  // },
  onReady: function () {
    // var self = this;
    // var mapidx = ['A', 'B', 'C', 'D', 'E', 'F'];
    // app.globalData.internal = setInterval(function doFrame() {
    //   global.drawImage("../img/BG_Sky_001.png", -2, 0);
    //   global.drawImage("../img/Panel_003.png", 0.07, 0.18);
    //   global.drawImage("../img/Role_001.png", 0.1, 0.22);
    //   var ilx = 0.43, ily = 0.39, ihy = 0.05;
    //   var bean = self.data.studentPlannedRouteTaskNodeRecordBean;

      
    //   if(bean){
    //     var title = bean.plannedRouteTaskNode.choiceQuestion.questionTitle;
    //     global.drawText(title, ilx - 0.05, ily - 0.1, 16,"#844F16");
    //     var opts = bean.plannedRouteTaskNode.choiceQuestion.choiceOptions;
    //     for (var k = 0; k < opts.length; k++) {
    //       var item = opts[k];
    //       global.drawText(item.optionItem, ilx, ily + ihy * k, 16, "#FFFFFF")
    //       var b2 = {x:ilx,y:ily+ihy*k-0.02,w:0.5,h:0.06};
    //       if (!self.data.btns["item"+k]) {
    //         self.data.btns["item" + k] = {
    //           bs: b2, onClick: (function () {
    //             self.data.selectOptinIndex = this;
    //           }).bind(k)
    //         }
    //       }
    //     }
    //   }
    //   if (self.data.selectOptinIndex!=undefined){
    //     global.drawImage("../img/Correct_001.png", 0.7, ily - 0.03 + self.data.selectOptinIndex*0.05);
    //   }
    //   var bn = global.drawImage("../img/Botton_018_Normal.png", 0.36, 0.6);
    //   self.data.btns["bn" ] = {
    //     bs: bn, onClick: (function () {
    //       self.onSumbit();
    //     }).bind(k)
    //   }
    //   wx.drawCanvas({canvasId: 1,actions: global.ctx.getActions()});
    // },0.0167);
  },
  /**
   * 更新内容
   */
  updataInfo:function(){
    var that = this;
    that.setData({
      studentPlannedRouteTaskNodeRecordBean : app.globalData.studentPlannedRouteTaskNodeRecordBean,
      fraction: app.globalData.studentPlannedRouteTaskNodeRecordBean.plannedRouteTaskNode.choiceQuestion.fraction,
      correctAnswerIndex: app.globalData.studentPlannedRouteTaskNodeRecordBean.plannedRouteTaskNode.choiceQuestion.correctAnswerIndex
    });
    that.updateSelectOption(app.globalData.studentPlannedRouteTaskNodeRecordBean.plannedRouteTaskNode.choiceQuestion.choiceOptions[0].optionIndex);
  }, 
  onSelectOption: function (e) {
    console.log(e.target.dataset.id);
    var that = this;
    that.updateSelectOption(e.target.dataset.id);
  },
  onBack:function(){
    util.Back(1);
    // util.navigateTo("../task/scantask",app);
  },
  /**
   * 更新 选中 的 选项
   */
  updateSelectOption:function(id){
    if (id || id == 0) {
      this.setData({
        selectOptinIndex: id,
      })
    }
  },
  /**
   * 提交
   */
  onSumbit:function(){
    var that = this;
    var taskScore = that.data.fraction;
    if (tool.isLoading(app)){
      return;
    }
    tool.showLoading(app,that);
    if (that.data.correctAnswerIndex != that.data.selectOptinIndex)
    {
      taskScore = 0;
    }
    var studentPlannedRouteTaskNodeRecordId = that.data.studentPlannedRouteTaskNodeRecordBean.studentPlannedRouteTaskNodeRecordId;
    app.globalData.taskScore = taskScore;
    app.globalData.lastTaskCount--;
    var url = app.data.apiUrl + "studentPlannedRouteTaskNodeRecords/" + studentPlannedRouteTaskNodeRecordId + "/submit/" + that.data.selectOptinIndex;
    var headr = util.getTokenHeadr(app);
    util.request(url, null, that.onSumbitCallBack,null,null,null,headr);
  },
  /**
   * 服务器提交响应
   */
  onSumbitCallBack:function(data,param){
    var that = this;
    tool.hideLoading(app,that);
    util.redirectTo('../task/nexttask',app);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // var that = this;
    // app.globalData.studentPlannedRouteTaskNodeRecordBean = testData.taskData();
    // that.updataInfo();
  },



  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.updataInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  },
  
})