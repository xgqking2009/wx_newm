// pages/task/scantask.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var testData = require('../../utils/testData.js')
//获取应用实例
const app = getApp()
Page({
  //courseApplyId=ff80808167a04e640167a65508a5000b&coursePlannedRouteId=ff80808167cf75070167cf939460001e&taskIndex=1
  /**
   * 页面的初始数据
   */
  data: { 
    indicatorDots: true,
    autoplay: false,
    interval: 5000,
    duration: 1000,
    circular: true,
    interval: 4000,
    boxheight: '',     
    curSelectTaskNodeID:null, //当前查看的节点ID
    curSelectTaskNode:null,//当前选择的节点数据
    routeName: "",  //路线名字
    studentPlannedRouteTaskNodeRecordBeanList: null, //节点列表
    finishCount:0,//完成打卡的数量
    scoreCount:0,//打卡获得的总分
    firstIndex:0, 
    curTaskIndex:1,
    isOpenScanPage:false,
    isLoading : false,
    titlex:250,
    btns:{}
  },
  onBack:function(){
    util.Back(1);
    // util.reLaunch("../task/selsec",app);
  },
  tapName: function (event) {
    var x = event.detail.x / global.vw, y = event.detail.y / global.vh;
    this.tap(x,y);
  }, 
  onLoad: function (options) {
    this.lstar = [0.02,0.03,0.025,0.023,0.02,0.02,0.01];
    global.btns = {};
  },
  onStart: function (event) {
    var x = event.touches[0].x/global.vw,y = event.touches[0].y/global.vh;
    this.tapx = x;this.tapy = y;
    this.mstate = 'touch';
    if(this.listUp) this.listUp.touchStart(x,y);
  },
  onMove: function (event) {
    this.mstate = 'move';
    var x = event.touches[0].x / global.vw, y = event.touches[0].y / global.vh;
    if (this.listUp) this.listUp.touchMove(x, y);
     
  },
  onEnd: function (event) {
    var x = event.changedTouches[0].x / global.vw, y = event.changedTouches[0].y / global.vh;
    if (this.listUp) this.listUp.touchEnd(x, y);
    var dx = x - this.tapx,dy = y - this.tapy;
    if(dx*dx+dy*dy<10000) global.tap(x,y);
  }, 
  onReady: function () {
    // var context = wx.createContext();
    // var self = this;
    // app.globalData.interval = setInterval(function doFrame() {
    //   var cm = global.drawImage("../img/BG_Sky_001.png", -2, 0);
    //   global.save(); 
    //   var bean = self.data.studentPlannedRouteTaskNodeRecordBeanList;
    //   if(bean){
    //     var iw = 0.3;
    //     var itw = iw * (bean.length - 1) + 0.12;
    //     if(!self.listUp)  self.listUp = new global.list(0.15,0.1,0.7,0.2,itw,0.2,'H');
    //     self.listUp.clip();
    //     var curSelectTaskNodeID = self.data.curSelectTaskNodeID;
    //     global.drawIOx = self.listUp.x;global.drawIOy = self.listUp.y;
    //     for (var k = 0; k < bean.length; k++) {
    //       var pwx = self.listUp.pwx() + iw * k, pwy = self.listUp.pwy();
    //       if(k!=bean.length-1) 
    //         global.drawImage("../img/Line_001.png", pwx+self.lstar[0], pwy + self.lstar[1], 0.3, 0.01);
    //       var item = bean[k];
    //       var idx = k + 1;
    //       var strnum = idx < 10 ? "0" + idx : ("" + idx),bs;
    //       var bkey = item.studentPlannedRouteTaskNodeRecordId;
    //       function onBtnClick(){
    //         var btn = global.btns[this];
    //         self.selectTaskNode(this);
    //         // self.data.curSelectTaskNodeID = this;
    //       }
    //       if (item.studentPlannedRouteTaskNodeRecordId == curSelectTaskNodeID) {
    //         global.canDrawImage = false;
    //         bs = global.drawImage("../img/Star_003.png", pwx, pwy);
    //         global.processBtn(bkey,bs,onBtnClick.bind(bkey));
    //         global.drawImage("../img/Num_" + strnum + ".png", pwx + self.lstar[2], pwy + self.lstar[3]);
    //       } 
    //       else {
    //         global.canDrawImage = false;
    //         bs = global.drawImage("../img/Star_001.png", pwx, pwy + self.lstar[6]);
    //         global.processBtn(bkey,bs,onBtnClick.bind(bkey));
    //         global.drawImage("../img/Num_" + strnum + ".png", pwx + self.lstar[4], pwy + self.lstar[5]);
    //       }
    //       // <!-- < block wx: for= "{{curSelectTaskNode.plannedRouteTaskNode.taskImageList}}" wx: key data - id='{{index}}' wx: for-item= "imgItem" > -->
    //       //   <!-- < swiper - item class= "swiperl" style = "height:12%;" > -->
    //       //     <!-- < image src = "https://cdn.studytour.info/58b5f541-4281-492e-9c29-0d765ced6aae.jpg"
    //       // style = "width:227px;height:112px;position:absolute;align-items:center;" /> -->
    //       //   <!-- < /swiper-item>  -->
    //       //   < !-- < /block> -->
    //       // global.canDrawImage = false;
    //       // var b1 = global.drawImage("../img/Botton_001_Normal.png", -1, 0.14 * global.s, cm);
    //       // var b2 = global.drawImage("../img/Botton_002_Normal.png", -1, 0.28 * global.s, cm);
    //       // global.canDrawImage = true;
    //       // var allbtns = {'b1':b1,'b2':b2};
    //       // global.addBtns(allbtns);
    //       // global.loopBtns();
    //     }
        
    //     global.restore();
    //     // global.drawImage(self.data.curSelectTaskNode.plannedRouteTaskNode.taskImageList[0],
    //     //   0.15, 0.22, 0.7, 0.25,5,);
    //     // global.drawTextInRect(self.data.curSelectTaskNode.plannedRouteTaskNode.description,
    //     //   0.15, 0.5, 0.7, 0.3, 3, 9,'#844F16')
        
    //   }
    //   global.canDrawImage = false;
    //   var cbn = global.drawImage("../img/Botton_017_Normal.png", 0.24, 0.86);
    //   global.canDrawImage = true;
    //   global.processBtn("cbn",cbn,
    //     function () {
    //         self.onScanCode();
    //   });
    //   global.drawImage("../img/Role_001.png", 0.35, 0.55);
    //   wx.drawCanvas({
    //     canvasId: 1,
    //     actions: global.ctx.getActions()
    //   });
    // }, 16.6);
  },
  /**
   * 获取当前的打卡节点列表
   */
  getTaskNodeList:function(callBackFunc){
    var that = this;
    if(tool.isLoading(app))
    {
      if(null != callBackFunc)
      {
        callBackFunc();
      }
      return;
    }
    tool.showLoading(app,that);
    var url = app.data.apiUrl + "studentCoursePlannedRouteRecords/" + app.globalData.studentCoursePlannedRouteRecordId+"/listTaskNodeRecords";
    var header = util.getTokenHeadr(app);
    util.request(url, null, that.getTaskNodeListCallBack, callBackFunc,null,'get',header);
  },
  /**
   * 服务器返回节点列表
   */
  getTaskNodeListCallBack(data,param){
    var that = this;
    tool.hideLoading(app,that);
    app.globalData.studentPlannedRouteTaskNodeRecordBeanList = data['responseBody']['result'];
    that.updataInfo();
    if(null != param)
    {
      param();
    }
  },


  /**
   * 刷新数据
   */
  updataInfo:function()
  {
    var that = this;
    var list = app.globalData.studentPlannedRouteTaskNodeRecordBeanList;
    var scoreCount = 0;
    var finishCount = 0;
    var item = null;
    var firstIndex = -1;
    var firstTaskNodeId=null;
    var lastTaskCount=0
    for (var i = 0; i < list.length; i++) {
      item = list[i];
      if (item.submit) {
        finishCount++;
        scoreCount += item.nodeScore;
      }
      else if (firstIndex < 0) {
        firstIndex = i;
        firstTaskNodeId = item.studentPlannedRouteTaskNodeRecordId;
        that.data.curTaskIndex = item.plannedRouteTaskNode.taskIndex;
        that.data.curTaskSumbit = item.plannedRouteTaskNode.submit;
      }
      item.plannedRouteTaskNode.taskImageList = item.plannedRouteTaskNode.taskImages.split(',');
    }
    lastTaskCount= list.length-finishCount;
    app.globalData.lastTaskCount = lastTaskCount;

    if (-1 == firstIndex) {
      firstIndex = 0;
      firstTaskNodeId = list[0].studentPlannedRouteTaskNodeRecordId;
      that.data.curTaskIndex = list[0].plannedRouteTaskNode.taskIndex;
    }

    that.setData({
      routeName: app.globalData.routeName,
      titlex: 375-app.globalData.routeName.length*12,
      studentPlannedRouteTaskNodeRecordBeanList: list,
      finishCount: finishCount,
      scoreCount: scoreCount
    });
    that.selectTaskNode(firstTaskNodeId);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    if (that.data.isOpenScanPage)
    {
      that.data.isOpenScanPage = false;
      return;
    }
    that.getTaskNodeList();
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    if (tool.isLoading(app)) {
      util.sotpPullDownRefresh();
      return;
    }
    that.getTaskNodeList(util.sotpPullDownRefresh);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  },
  /**
   * 切换节点
   */
  onChangStyle:function(e){
    console.log(e.target.dataset.id);
    var id = e.target.dataset.id;
    var that = this;
    var taskIndex = e.target.dataset.tid;
    that.data.curTaskIndex = taskIndex;
    that.selectTaskNode(id);
      
  },
  /**
   * 选择打卡节点ID
   */
  selectTaskNode:function(nodeId)
  {
    console.log("Xgq selectTaskNode.....");
    var that = this;
    if (nodeId || nodeId == 0) {
      var studentPlannedRouteTaskNodeRecordBeanList = that.data.studentPlannedRouteTaskNodeRecordBeanList;
      var item = null;
      var curItem = null;
      for (var i = 0; i < studentPlannedRouteTaskNodeRecordBeanList.length; i++) {
        item = studentPlannedRouteTaskNodeRecordBeanList[i];
        if (item.studentPlannedRouteTaskNodeRecordId == nodeId) {
          curItem = item;
          break;
        }
      }
      app.globalData.studentPlannedRouteTaskNodeRecordBean = curItem;
      this.setData({
        curSelectTaskNodeID: nodeId,
        curSelectTaskNode: curItem,
        curTaskSumbit:curItem.submit
      })
    }
  },
  onScanCode(){
    var that = this;
    if (that.data.curTaskSumbit==true) {
      tool.showToast('已经扫码探究过了', 'cancel');
      return;
    }
    that.data.isOpenScanPage = true;
    wx.scanCode({
      success:(res)=>{
        var result = res['result'];
        var tmpArr = result.split('&');
        var obj={};
        for(var i=0;i<tmpArr.length;i++)
        {
          var arr = tmpArr[i].split('=');
          obj[arr[0]] = arr[1];
        }
        obj['taskIndex'] = parseInt(obj['taskIndex']);
        // util.reLaunch('../task/task', app); 
        // return;
        // console.log("Xgq courseApplyId:"+app.globalData.courseApplyId+" "+
        //   app.globalData.coursePlannedRouteId + " " + that.data.curTaskIndex);
        if (  obj['courseApplyId'] == app.globalData.courseApplyId && 
              obj['coursePlannedRouteId'] == app.globalData.coursePlannedRouteId &&
              obj['taskIndex'] == that.data.curTaskIndex)
          { 
            clearInterval(app.globalData.interval);
            util.reLaunch('../task/task',app); 
          } 
          else
          {
            wx.showToast({
              title: '   二维码不对！！！',
              image: '../img/error.png',
              duration: 2000
            });
          // tool.showToast('二维码不对！！！','error');
          }
        console.log(res);
      },
      fail:(res)=>{
        console.log(res);
      }
    })
  }
})