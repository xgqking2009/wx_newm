// pages/task/finshtask.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrolls: [{ num: 1, nums: 0 }, { num: 2, nums: 1 }, { num: 3, nums: 2 }],
    ids:0,
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  },
  changstyle: function (e) {
    //console.log(e.target.dataset.nums);
    if (e.target.dataset.nums || e.target.dataset.nums == 0) {
      this.setData({
        ids: e.target.dataset.nums,
      })
    }

  },
})