// pages/task/selsec.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
//获取应用实例
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */ 
  data: {
    indicatorDots: true,
    autoplay: false,
    interval: 5000,
    duration: 1000,
    circular: true,
    interval: 4000,
    boxheight: '',
    btns:{}, 
    isLoading:false, //是否在加载数据中
    isBlankLine:false,
    coursePlannedRouteBeanList:null,   //任务路线列表
    curSelectPlannedID:null,  //当前选择的路线
    curSelectPlanned:null,//当前选择的内容
    oldSelectPlannedId:null, //之前选择的路线
    isScanTask:false, 
    isSelectPage:false, //当前是否是选择页面
    mapn:["A","B","C"],
    maps:["Selected","Normal"] 
  },
  tapName: function (event) {
    var x = event.detail.x / global.vw, y = event.detail.y / global.vh;
    var btns = this.data.btns;
    for (var k in btns) {
      var bs = btns[k].bs; 
      if (x >= bs.x && x < bs.x + bs.w && y >= bs.y && y < bs.y + bs.h) {
        btns[k].onClick();
      }
    }
  },
  onReady: function () {
    util.hideLoading();
    // var self = this;
    // var context = wx.createContext(); 
    // self.interval = setInterval(function doFrame() {
    //   var cbl = self.data.coursePlannedRouteBeanList;
    //   var cs = app.globalData.curSelectPlanned;
    //   if(cs){ 
    //     global.drawImage("../img/BG_Sky_001.png", -2, 0);
    //     var back = global.drawImage("../img/Botton_Return_Normal.png", 0, 0.05);
    //     if (!self.data.btns.back) {
    //       self.data.btns.back = {
    //         bs: back, onClick: function () {
    //           clearInterval(self.interval);
    //           util.navigateTo("../user/infos", app);
    //         } 
    //       }
    //     }  

    //     for(var k = 0;k<cs.plannedRouteTaskNodes.length;k++){
    //       var item = cs.plannedRouteTaskNodes[k];
    //       if(item.tmpPath){
    //         global.drawImage(item.tmpPath, 0.13, 0.35,0.74,0.2);
    //       }
    //     }
    //     global.drawText(cs.routeName, 0.4, 0.3, 20, "#844F16");
    //     global.flush();
    //   }
    // }, 16.6);
  }, 
/**
 * 获取指定课程所有路线数据
 */ 
  onScanTask:function(){
    util.navigateTo('../task/scantask',app);
  }, 
  onBack:function(){
    app.globalData.appState = "route";
    //util.Back(1); 
    util.reLaunch('../user/course',app); 
  },
  getCoursePlanneData: function ()
  {
    var that = this;
    if(tool.isLoading(app))
    {
      return;  
    }
    tool.showLoading(app,that);
    var courseApplyId = app.globalData.courseApplyId;
    var url = app.data.apiUrl + "coursePlannedRoutes/courseApply/"+courseApplyId;
    var header = util.getTokenHeadr(app);
    console.log(">>>>>>>>>选择路线请求数据：\n\t\t" + url);
    util.request(url, null, that.GetCoursePlanneDataCallBack, null, null, 'get', header);
  },
  /**
   * 从服务器获取课程数据完成
   */
  GetCoursePlanneDataCallBack:function(data)
  {
    var that = this;
    tool.hideLoading(app, that);
    var coursePlannedList = data['responseBody']['result'];
    app.globalData.coursePlannedRouteBeanList = coursePlannedList;
    console.log("<<<<<<<<<<选择路线返回数据：\n\t\t" + JSON.stringify(coursePlannedList));
    that.updataData();
  },
  /**
   * 更新数据 
   */ 
  updataData:function()
  {
    var that = this;
    var list = app.globalData.coursePlannedRouteBeanList;
    if(null == list) 
    {
      return;
    }
    var curSelectID= null;
    var curSelectData = null;
    for(var i=0;i<list.length;i++)
    {
      if (list[i].coursePlannedRouteId == app.globalData.coursePlannedRouteId)
      {
        curSelectData = list[i];
        curSelectID = curSelectData.coursePlannedRouteId;
        break;
      }
    }
    var item = null;
    var plannedRouteTaskNodes = null;
    var node = null;
    for(var i=0;i<list.length;i++)
    {
      item = list[i];
      plannedRouteTaskNodes = item.plannedRouteTaskNodes;
      for (var j = 0; j < plannedRouteTaskNodes.length;j++){
        node = plannedRouteTaskNodes[j];
        node.taskImageList = node.taskImages.split(',');
      }
    }
    if(curSelectData == null)
    {
      curSelectData = list[0]; 
      if(curSelectData==undefined){
        that.setData({
          isBlankLine:true
        })
        return;
      }else{
        that.setData({
          isBlankLine:false
        })
      }
      curSelectID = curSelectData.coursePlannedRouteId;
    }
    app.globalData.curSelectPlanned = curSelectData;
    var cs = curSelectData;
    if (!cs.loaded) {
      var imgs = {};
      var loadtask = [];
      for (var k = 0; k < cs.plannedRouteTaskNodes.length; k++) {
        var item = cs.plannedRouteTaskNodes[k];
        if(!imgs[item.taskImages])
        { 
          imgs[item.taskImages] = 'loaded';
          loadtask.push(item);
        }
      }

      function loadImage(loadtask){
        if(loadtask.length==0) return;
        wx.getImageInfo({
          src: loadtask[0].taskImages,
          success: function (res) {
            loadtask.splice(0,1);
            loadImage(loadtask); 
            item.tmpPath = res.path;
            global.imginfo[item.tmpPath] = [res.width / 2, res.height / 2];
          },    
          fail: function (res) { 
            loadImage(loadtask);
          } 
        });
      }; 
      loadImage(loadtask);
      cs.loaded = true;
    }
    that.setData({
      curSelectPlannedID: curSelectID,
      oldSelectPlannedId: app.globalData.coursePlannedRouteId,   
      coursePlannedRouteBeanList: app.globalData.coursePlannedRouteBeanList,
      curSelectPlanned: curSelectData,
      isSelectPage: app.globalData.isSelectPage,
      isScanTask: app.globalData.isScanTask
    });
  },
   
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isSelectPage: app.globalData.isSelectPage,
      isScanTask: app.globalData.isScanTask
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getCoursePlanneData();
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  },
  selectLineCallServer:function()
  {
    var that = this;
    if (tool.isLoading(app)) {
      return;
    }
    tool.showLoading(app, that);
    var url = app.data.apiUrl + "/studentCoursePlannedRouteRecords/" + app.globalData.studentCoursePlannedRouteRecordId + "/chooseCoursePlannedRoute/" + that.data.curSelectPlannedID;
    var header = util.getTokenHeadr(app);
    util.request(url, null, that.selectCallBack, null, null, null, header);
  },
  /**
   * 选择路线
   */
  onSelectTask:function(e){
    var that = this;
    that.selectLineCallServer();
  }, 
  /**
   * 修改路线 
   */
  onChangeTask:function(e){
    var that = this;
    tool.showModal('取消当前选择，重新修改？', null, that.onSelectConfirm, that.onSelectCancel);
  },
  /**
   * 确认选择
   */
  onSelectConfirm()
  {
    var that = this;
    that.selectLineCallServer();
  },
  /**
   * 服务器 修改路线返回
   */
  selectCallBack(data,param){
    var that = this;
    tool.hideLoading(app,that);
    app.globalData.appState = "route";
    util.redirectTo('../user/course',app);
  },
  /**
   * 取消选择
   */
  onSelectCancel(){

  },
  /**
   * 进入打卡
   */
  onEnterTask:function(e){
    var that = this;
    if (app.globalData.enterTaskNeedConfirm)
    {
      tool.showModal('确定进入 ' + app.globalData.routeName + ' 路线，一旦确定则不可修改', null, that.onEnterConfirm, that.onEnterCancel);
    }
    else
    {
      that.onEnterConfirm();
    }
    
  },
  onEnterConfirm:function(){
    var that = this;
    if (tool.isLoading(app)) {
      return;
    }
    tool.showLoading(app, that);
    var url = app.data.apiUrl + "studentCoursePlannedRouteRecords/" + app.globalData.studentCoursePlannedRouteRecordId + "/startedStudentCoursePlannedRouteRecord";
    var header = util.getTokenHeadr(app);
    util.request(url, null, that.onEnterCallBack, null, null, null, header);
  },
  onEnterCancel:function(){

  },
  /**
   * 进入打开 服务器返回
   */
  onEnterCallBack:function(data,param)
  {
    var that = this;
    tool.hideLoading(app, that);
    console.log("Xgq scantask.....1");
    util.redirectTo('../task/scantask', app);
  },
  /**
   * 选择切换路线
   */
  onChangstyle:function(e){
    var that = this;
    var idName = e.target.dataset.id;
    var coursePlannedRouteBeanList = that.data.coursePlannedRouteBeanList;
    var selectPlanned = null;
    if (null == coursePlannedRouteBeanList)
    {
      return;
    }
    for (var i = 0; i < coursePlannedRouteBeanList.length; i++)
    {
      if (coursePlannedRouteBeanList[i].coursePlannedRouteId == idName)
      {
        selectPlanned = coursePlannedRouteBeanList[i];
        break;
      }
    }
    console.log('curSelectPlannedID: ' + idName);
    console.log('selectPlanned.name: ' + selectPlanned.routeName);
    app.globalData.routeName = selectPlanned.routeName;
    this.setData({
      curSelectPlannedID: idName,
      curSelectPlanned: selectPlanned
    })
  }
})