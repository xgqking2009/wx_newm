// pages/login/phlogin.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var timer = 1
//获取应用实例
const app = getApp()
Page({
  /**
  * 页面的初始数据
  */
  data: {
    iv: null,
    encryptedData: null,
    rawData: null,
    signature: null,
    gsl:global.sl,
    tops:[],
    wxappName: '小程序名字',//小程序名字
    phoneNumber: null, //手机号码
    userInfo: null,
    hasUserInfo: false,
    password: null,          //密码
    code: null,              //验证码
    confirmPassword: null,   //确认密码
    windowWidth: 0,//这个指屏幕的宽度
    windowHeight: 0,
    nextButtonWidth: 0,
    getmsg: "获取验证码", //这个指button——获取验证码的显示的文字

    disabled_code:false,//点击获取验证码后 按钮要不可用
    
    timeInter:null,  //计时器 页面跳转的时候要关闭

    isLoading: false,

    isDoFunc:false,
  },
  onBack:function(){
    util.Back(1);
    // util.navigateTo("../index/index",app);
  },
  /**
  * 生命周期函数--监听页面初次渲染完成
  */
  onReady: function () {
    var mx = 80, my = 0;
    var self = this;
    setTimeout(function doFrame() {
      global.drawImage("../img/BG_Sky_001.png", -2, 0);
      global.drawImage("../img/Botton_Return_Normal.png", 0, 0.05);
      var cm = global.drawImage("../img/Panel_002.png", -1, 0.3);
      var c1 = global.drawImage("../img/Inputbox_001.png", cm.x + 0.1, cm.y + 0.13);
      var c2 = global.drawImage("../img/Inputbox_001.png", cm.x + 0.1, cm.y + 0.23);
      
      var c21 = global.drawImage("../img/Botton_003_Normal.png", cm.x + 0.42, cm.y + 0.23 + 0.005);
      var c3 = global.drawImage("../img/Inputbox_002.png", cm.x + 0.1, cm.y + 0.33);
      global.drawImage("../img/Botton_005_Normal.png", cm.x+0.6, cm.y + 0.33);
      global.drawImage("../img/Botton_009_Normal.png", cm.x + 0.7, cm.y + 0.05);
      self.setData({
        tops: [c1.y * 100,c2.y*100+0.25]
      })
      wx.drawCanvas({
        canvasId: 1,
        actions: global.ctx.getActions()
      });
    }, 16.6);
  },
  /**
   * 手机号码输入
   */
  onPhoneTxt:function(e)
  {
    var that = this;
    that.data.phoneNumber = e.detail.value;
    console.log('that2222.data.phoneNumber' + that.data.phoneNumber);
  },
  
  /**
   * 输入验证码
   */
  onCodeTxt:function(e)
  {
    var that = this;
    that.data.code = e.detail.value;
    console.log('that.data.phoneNumber' + that.data.phoneNumber);
  },

  /**
  * 获取验证码
  */
  formSubmit_phone: function (e) {
    var that = this;
    var bol = tool.validatemobile(that.data.phoneNumber);
    if(bol==true)
    {
      that.setData({
        disabled_code:true,
      });
      var url = app.data.apiUrl +"customerUsers/sendLoginCode";
      util.request(url, { phoneNumber: that.data.phoneNumber }, that.getCodeCallBack,null,null,'get');
    }
    else
    {
      that.data.phoneNumber = null;
    }
    //获取手机验证码
  },
  /**
   * 获取短信验证码
   */
  getCodeCallBack:function(res)
  {
    var that = this;
    /**
     * errorCode:null
        errorMsg:null
        responseBody:"2941"
        responseStatus:"succeed"
        responseTime:1543995377196
     */
    if (res['responseStatus'] == 'succeed')
    {
      that.openTimer();
    }
  },

  /**
   * 点击跳转注册
   */
  toreg: function (event) {
    wx.navigateTo({
      url: 'phregist',
    })
  },
  /**
   * 获取用户信息
   */
  onGotUserInfo: function (e) {
    var that = this;
    if (e.detail.errMsg != null && e.detail.errMsg == "getUserInfo:fail user deny") {
      return;
    }
    that.data.encryptedData = e.detail.encryptedData;
    that.data.iv = e.detail.iv;
    that.data.rawData = e.detail.rawData;
    that.data.signature = e.detail.signature;
    util.getUserInfo(app, that);
  },

  /**
   * 获取用户信息完成
   */
  getUserInfoCallBack: function () {
    var that = this;
    that.data.userInfo = app.globalData.userInfo;
    var url = app.data.apiUrl + "wxMiniApp/userInfo";
    var data = {
      sessionKey: app.globalData.sessionKey,
      encryptedData: that.data.encryptedData,
      iv: that.data.iv,
      rawData: that.data.rawData,
      signature: that.data.signature
    };
    util.request(url, data, that.userInfoServerCallBack, null, null, 'get');
    
  },

  loginByPassword:function(){
    var that = this;
    var url = app.data.apiUrl + "users/loginByPassword";
    var data = {
      account: that.data.phoneNumber,
      password: that.data.code
    };
    util.request(url, data, that.userLoginBack, null, null);
  },
  /**
   * 服务器获取用户信息返回
   */
  userLoginBack:function(data,param){
    var that = this;
    tool.hideLoading(app, that);
    var customerUserBean = data['responseBody'];
    if(customerUserBean.userId){
      app.globalData.phoneNumber = that.data.phoneNumber;
      app.globalData.customerUserBean = customerUserBean;
      app.globalData.token = customerUserBean.token.newToken;
      util.reLaunch('../user/infos', app);
    }
  },
  userInfoServerCallBack:function(data,param){
    var that = this;
    app.globalData.userInfo = data['responseBody'];
    that.setData({
      userInfo: app.globalData.userInfo,
    });
    if(!that.data.isDoFunc)
    {
      that.doSubmitLogin();
      that.data.isDoFunc = true;
    }
    
  },

  /**
   * 提交注册
   */
  formSubmit_login: function () {
    var that = this;
    var userInfo = app.globalData.userInfo;
    if (null == userInfo) {
      return;
    }
    that.data.isDoFunc = true;
    that.doSubmitLogin();
  },

  /**
   * 提交登陆
   */
  doSubmitLogin: function () {
    var nickName;
    var that = this;
    var bol = false;
    bol = tool.validatemobile(that.data.phoneNumber);
    if (bol==true){
      // bol = tool.validatecode(that.data.code);
    }
    console.log("Xgq bol:"+bol);
    if (bol==false){
      return;
    } 
    if (null != that.data.userInfo && null != that.data.userInfo.nickName) {
      nickName = that.data.userInfo.nickName;
    }
    if (null != that.data.phoneNumber && null != that.data.code) {
      if(tool.isLoading(app))
      {
        return;
      }
      tool.showLoading(app,that);
      
      var url = app.data.apiUrl +"customerUsers/loginBySmsCode";
      var data = {
        account: that.data.phoneNumber, //账号 手机号
        smsCode: that.data.code, //验证码
        weXinMiniAppOpenid: app.globalData.openID, //微信小程序的openid
        weXinNickName: app.globalData.userInfo.nickName,
        weXinAvatarUrl: app.globalData.userInfo.avatarUrl, //微信头像
      };
      util.request(url, data, that.loginCallBack);
    }
  },
  /**
   * 登录返回
   */
  loginCallBack:function(data,param)
  {
    var that = this;
    tool.hideLoading(app,that);
    var customerUserBean = data['responseBody'];
    app.globalData.phoneNumber = that.data.phoneNumber;
    app.globalData.customerUserBean = customerUserBean;
    app.globalData.token = customerUserBean.token.newToken;
    util.reLaunch('../user/infos', app);
  },

  /**
   * 打开注册页面
   */
  onGotoRegist:function()
  {
    util.navigateTo('../login/phregist', app);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.getSystemInfo({
      success: function (res) {                      //获取用户的手机的型号的信息，
        that.setData({
          wxappName:app.globalData.wxappName,
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight,
          nextButtonWidth: res.windowWidth - 20
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    var that = this;
    that.cleanTimer();
  },
  /**
   * 关闭验证码计时器
   */
  cleanTimer:function()
  {
    var that = this;
    if (that.data.timeInter != null) {
      clearInterval(that.data.timeInter);
      that.data.timeInter = null;
    }
  },
  /**
   * 开启验证码计时器
   */
  openTimer:function()
  {
    if (timer == 1) {
      timer = 0
      var that = this
      var time = 10
      that.data.timeInter = setInterval(function () {
        that.setData({
          getmsg: time + "s后重新发送",
        })
        time--
        if (time < 0) {
          timer = 1
          clearInterval(that.data.timeInter);
          that.data.timeInter = null;
          that.setData({
            getmsg: "获取短信验证码",
            disabled_code: false,
          })
        }
      }, 1000)
    }
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  }
})