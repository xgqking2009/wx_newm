// pages/login/wxlogin.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var timer = 1
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    iv: null,
    encryptedData: null,
    rawData:null,
    signature:null,

    wxappName: '小程序名字',//小程序名字
    phoneNumber: null, //手机号码
    userInfo: null,
    hasUserInfo: false,
    password: null,          //密码
    code: null,              //验证码
    windowWidth: 0,//这个指屏幕的宽度
    windowHeight: 0,
    nextButtonWidth: 0,
    getmsg: "", //这个指button——获取验证码的显示的文字

    disabled_code: false,//点击获取验证码后 按钮要不可用

    timeInter: null,  //计时器 页面跳转的时候要关闭
    pwd: null,//密码
    rePwd: null,//确认密码

    isLoading: false,

    isDoFunc: false,
  },

  onBack:function(){
    util.Back(1);
  },
  /**
   * 输入验证码
   */
  onCodeTxt: function (e) {
    var that = this;
    that.data.code = e.detail.value;
    console.log('that.data.phoneNumber' + that.data.phoneNumber);
  },
  /**
   * 密码
   */
  onPwdTxt: function (e) {
    var that = this;
    that.data.pwd = e.detail.value;
  },  
  /**
   * 确认密码
   */
  onRePwdTxt: function (e) {
    var that = this;
    that.data.rePwd = e.detail.value;
  },

  /**
  * 获取验证码
  */
  onSendmessg: function (e) {
    
    var that = this;
    console.log("Xgq that.data.getmsg:" + that.data.disabled_code + " app.globalData.phoneNumber:" + app.globalData.phoneNumber);
    if (that.data.disabled_code==true){
      wx.showToast({
        title: '请稍后重试',
        icon: 'info',
        duration: 1500 
      })
      return;
    }
    console.log("onSubmitCode");
    var bol = tool.validatemobile(app.globalData.phoneNumber);
  
    if (bol) {
      that.setData({
        disabled_code: true,
      });
      var url = app.data.apiUrl + "customerUsers/sendRegisterCode";
      console.log("Xgq codeurl:"+url); 
      util.request(url, { phoneNumber: app.globalData.phoneNumber }, that.getCodeCallBack, null, null, 'get');
    }
    else {
      that.data.phoneNumber = null;
    }
    //获取手机验证码
  },
  /**
   * 获取短信验证码
   */
  getCodeCallBack: function (res) {
    var that = this;
    /**
     * errorCode:null
        errorMsg:null
        responseBody:"2941"
        responseStatus:"succeed"
        responseTime:1543995377196
     */
    if (res['responseStatus'] == 'succeed') {
      that.openTimer();
    }
  },
  /**
     * 获取用户信息
     */
  onGotUserInfo: function (e) {
    var that = this;
    if (e.detail.errMsg != null && e.detail.errMsg == "getUserInfo:fail user deny") {
      return;
    }
    that.data.encryptedData = e.detail.encryptedData;
    that.data.iv = e.detail.iv;
    that.data.rawData = e.detail.rawData;
    that.data.signature = e.detail.signature;
    util.getUserInfo(app, that);
  },

  /**
   * 获取用户信息完成
   */
  getUserInfoCallBack: function () {
    var that = this;
    var url = app.data.apiUrl +"wxMiniApp/userInfo";
    var data = {
      sessionKey: app.globalData.sessionKey,
      encryptedData: that.data.encryptedData,
      iv:that.data.iv,
      rawData: that.data.rawData,
      signature: that.data.signature
    };
    util.request(url, data, that.userInfoServerCallBack,null,null,'get');
    
  },
  /**
   * 服务器返回用户信息
   */
  userInfoServerCallBack:function(data,param)
  {
    var that = this;
    app.globalData.userInfo = data['responseBody'];
    that.setData({
      userInfo: app.globalData.userInfo,
    });
    var nickName = that.data.userInfo.nickName;
    console.log('nickname: ' + that.data.userInfo.nickName);
    if (!that.data.isDoFunc)
    {
      that.doSubmitRegist();
      that.data.isDoFunc = true;
    }
    
  },

  /**
   * 提交注册
   */
  formSubmit_reg:function(){
    var that = this;
    var userInfo = app.globalData.userInfo;
    if (null == userInfo) {
      return;
    }
    // console.log("Xgq formSubmit_reg:"+JSON.stringify(userInfo));
    that.data.userInfo = userInfo;
    that.data.isDoFunc = true;
    that.doSubmitRegist();
  },

  /**
   * 提交登陆
   */
  doSubmitRegist: function () {
    var nickName;
    var that = this;

    var nickName = that.data.userInfo.nickName;
    console.log('nickname: ' + that.data.userInfo.nickName+"that.data.pwd:"+that.data.pwd+" rePwd:"+that.data.rePwd);
    var bol = false;
    bol = tool.validatecode(that.data.code);
    if (!bol) {
      wx.showToast({
        title: '验证码无效！',
        image: '../img/error.png',
        duration: 2000
      });   
      return;
    }
    if (bol) {
      if (that.data.pwd == null || (that.data.pwd && that.data.pwd.length < 6)) {
        wx.showToast({
          title: '密码设置至少6位！',
          image: '../img/error.png',
          duration: 2000
        });
        return;
      } else if (that.data.pwd != that.data.rePwd) {
        console.log("Xgq pass !=====");
        wx.showToast({
          title: '两次密码不一致！',
          image: '../img/error.png',
          duration: 2000
        });
        return;
      } 
      bol = that.data.pwd != null && that.data.pwd.length > 3 && that.data.pwd == that.data.rePwd;
    }
    if (null != that.data.userInfo && null != that.data.userInfo.nickName) {
      nickName = that.data.userInfo.nickName;
    }  
    if (null != app.globalData.phoneNumber && null != that.data.code) {
      if(tool.isLoading(app))
      {
        return; 
      }
      tool.showLoading(app,that);
      var url = app.data.apiUrl + "customerUsers/registerBySmsCode";
      var data = {
        account: app.globalData.phoneNumber, //账号 手机号
        password: that.data.pwd,
        smsCode: that.data.code, //验证码
        weXinMiniAppOpenid: app.globalData.openID, //微信小程序的openid
        weXinNickName: that.data.userInfo.nickName,
        weXinAvatarUrl: that.data.userInfo.avatarUrl, //微信头像
      };
      console.log("Xgq registCallBack url:"+url);
      util.request(url, data, that.registCallBack);
    }
  }, 
  /** 
   * 登录返回 
   */  
  registCallBack: function (res) {
    var that = this;
    tool.hideLoading(app,that);
    var customerUserBean = res['responseBody'];
    if (customerUserBean.userId) {
      app.globalData.phoneNumber = that.data.phoneNumber;
      app.globalData.customerUserBean = customerUserBean;
      app.globalData.token = customerUserBean.token.newToken;
      util.reLaunch('../user/infos', app);
    }else{
      wx.showToast({
        title: '无效的验证码',
        icon: 'info',
        image: '../img/error.png',
        duration: 1500
      })
    }
  },

  /**
   * 关闭验证码计时器
   */
  cleanTimer: function () {
    if (that.data.timeInter != null) {
      clearInterval(that.data.timeInter);
      that.data.timeInter = null;
    }
  },
  /**
   * 开启验证码计时器
   */
  openTimer: function () {
    if (timer == 1) {
      timer = 0
      var that = this
      var time = 10
      that.data.timeInter = setInterval(function () {
        that.setData({
          getmsg: time + "s后重新发送",
        })
        time--
        if (time < 0) {
          timer = 1
          clearInterval(that.data.timeInter);
          that.data.timeInter = null;
          that.setData({
            getmsg: "",
            disabled_code: false,
          })
        }
      }, 1000)
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.setData({
      wxappName: app.globalData.wxappName,
      phoneNumber: util.formatPhoneNum(app.globalData.phoneNumber),
    });
    that.onSendmessg(null);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  }
})