// pages/sort/sort.js
var util = require('../../utils/util.js')
var tool = require('../../utils/tool.js')
var testData = require('../../utils/testData.js')
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrolls: [{ num: 1, score: 100 }, { num: 2, score: 99 }, { num: 3, score: 98}],
    rankStudentCoursePlannedRouteRecordBeanList:null,//排名数据
    myRankStudentCoursePlannedRouteRecordBean:null,//自己的排名数据
    userInfo:null,
    courseApplyName:null,//课程名称
    defaultImg:"../img/tx_03.png",
    vW:0,
    vH:0,
    pR:0
  },
  onBack:function(){
    util.Back(1);
  },
  requestDataFromServer: function (callBackFunc = null){
    var that = this;
    if(tool.isLoading(app))
    {
      if(null != callBackFunc)
      {
        callBackFunc();
      }
      return;
    }
    tool.showLoading(app,that);
    var url = app.data.apiUrl + 'studentCoursePlannedRouteRecords/sortRank/' + app.globalData.customerUserOrdersCourseId;
    var header = util.getTokenHeadr(app);
    util.request(url, null, that.requestDataSeverCallBack,callBackFunc,null,'get',header);
  },
  requestDataSeverCallBack:function(data,param){
    var that = this;
    tool.hideLoading(app,that);
    var coursePlannedList = data['responseBody']['result'];
    app.globalData.rankStudentCoursePlannedRouteRecordBeanList = coursePlannedList;
    console.log("Xgq app.globalData.rankStudentCoursePlannedRouteRecordBeanList:" + JSON.stringify(app.globalData.rankStudentCoursePlannedRouteRecordBeanList));
    that.updataInfo();
    if(null != param)
    {
      param();
    }
  },

  updataInfo:function(){
    var that = this;
    var list = app.globalData.rankStudentCoursePlannedRouteRecordBeanList;
    var item = null;
    var myItem = null;
    for(var i=0;i<list.length;i++)
    {
      item = list[i];
      if (item.phoneNumber == app.globalData.phoneNumber)
      { 
        myItem = item;
        break;
      }
      if (item.avatarUrl == null || item.avatarUrl.length<=1){
        item.avatarUrl = that.data.defaultImg;
      }
    }
    that.setData({
      rankStudentCoursePlannedRouteRecordBeanList: list,
      myRankStudentCoursePlannedRouteRecordBean:myItem,
      userInfo: app.globalData.userInfo,
      courseApplyName: app.globalData.courseApplyName,
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      vw:global.vw,
      vh:global.vh,
      pr:global.pr
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.requestDataFromServer();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    if (tool.isLoading(app)) {
      util.sotpPullDownRefresh();
      return;
    }
    that.requestDataFromServer(util.sotpPullDownRefresh);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return util.onShareApp();
  }
})