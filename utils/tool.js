function validatemobile(mobile)
 {
   console.log("Xgq mobile:"+mobile);
  if (mobile == null || mobile.length == 0) {
    wx.showToast({
      title: '请输入手机号！',
      icon: 'info',
      duration: 1500
    })
    return false; 
  }
  if (mobile.length != 11) {
    wx.showToast({
      title: '手机号长度有误！',
      icon: 'info',
      duration: 1500
    })
    return false;
  }
  var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
  if (!myreg.test(mobile)) {
    wx.showToast({
      title: '手机号有误！',
      icon: 'info',
      duration: 1500
    })
    return false;
  }
  return true;
}

function validatecode(code)
{
  if(code == null || code.length == 0)
  {
    wx.showToast({
      title: '请输入验证码！',
      icon:'info',
      duration : 1500
    })
    return false;
  }
  return true;
}
/**
 * 是否在加载中
 */
function isLoading(app)
{
  return app.globalData.isLoading;
}
/**
 * 显示加载等待
 */
function showLoading(app,that = null,message='')
{
  if (app.globalData.isLoading)
  {
    return;
  }
  app.globalData.isLoading = true;
  if (wx.showLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.showLoading({
      title: message,
      mask: true
    });
  } else {
    // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
    wx.showToast({
      title: message,
      icon: 'loading',
      mask: true,
    });
  }
  if(null != that)
  {
    that.setData({
      isLoading : true,
    });
  }
}
/**
 * 关闭加载等待
 */
function hideLoading(app,that)
{
  if (!app.globalData.isLoading)
  {
    return;
  }
  app.globalData.isLoading = false;
  if (wx.hideLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.hideLoading();
  } else {
    wx.hideToast();
  }
  if (null != that) {
    that.setData({
      isLoading: false,
    });
  }
}
/**
 * 停止下拉更新
 */
function sotpPullDownRefresh(){
  wx.stopPullDownRefresh();
}

function showModal(content, title = '',  confirm = null,cancle = null)
{
  if(title ==  null)
  {
    title = '';
  }
  wx.showModal({
    title: title,
    content: content,
    success: function (res) {
      if (res.confirm) {//这里是点击了确定以后
        if (null != confirm)
        {
          confirm();
        }
      } else {//这里是点击了取消以后
        if(null != cancle)
        {
          cancle();
        }
      }
    }
  })
}

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}


function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/** 
 * 时间戳转化为年 月 日 时 分 秒 
 * number: 传入时间戳 
 * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 Y/M/D h:m:s
*/
function formatTimeTwo(number, format) {

  var formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
  var returnArr = [];

  var date = new Date(number );
  returnArr.push(date.getFullYear());
  returnArr.push(formatNumber(date.getMonth() + 1));
  returnArr.push(formatNumber(date.getDate()));

  returnArr.push(formatNumber(date.getHours()));
  returnArr.push(formatNumber(date.getMinutes()));
  returnArr.push(formatNumber(date.getSeconds()));

  for (var i in returnArr) {
    format = format.replace(formateArr[i], returnArr[i]);
  }
  return format;
}

/**
 *  Y/M/D
 */
function formatTimeYMD(number, format) {

  var formateArr = ['Y', 'M', 'D'];
  var returnArr = [];

  var date = new Date(number );
  returnArr.push(date.getFullYear());
  returnArr.push(formatNumber(date.getMonth() + 1));
  returnArr.push(formatNumber(date.getDate()));

  for (var i in returnArr) {
    format = format.replace(formateArr[i], returnArr[i]);
  }
  return format;
}

function showToast(title, icon ='success' ,duration=2000){
  wx.showToast({
    title: title,
    icon:icon,
    duration: duration
  });
}

module.exports = {
  validatemobile: validatemobile,
  validatecode: validatecode,
  showLoading: showLoading,
  hideLoading: hideLoading,
  isLoading: isLoading,
  sotpPullDownRefresh: sotpPullDownRefresh,
  showModal: showModal,
  formatTime: formatTime,
  formatTimeTwo: formatTimeTwo,
  formatTimeYMD: formatTimeYMD,
  showToast: showToast
}