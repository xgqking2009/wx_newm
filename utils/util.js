const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 * 获取带token的头部
 */
const getTokenHeadr= app=>{
  return {
    'content-type': 'application/json', // 默认值
    'new-token': app.globalData.token,
  };
}
/**
 * 和服务器请求数据
 */
const request = (url, data, success, param = null, failure = null, method = null,header = null) =>{
  if(header == null)
  {
    header = {
      'content-type': 'application/json' // 默认值
    };
  }
  if(method == null)
  {
    method = 'post';
  }
  wx.request({
    url: url,
    data:data,
    method: method,
    header: header,
    success: function (res) {
      
      if (res.data.responseStatus == "failed")
      {
        var errMsg = res.data.errorMsg;
        if(null == errMsg)
        {
          errMsg = res.data.errorCode;
        }
        if(null == errMsg)
        {
          errMsg = 'failed';
        }
        wx.showToast({
          title: errMsg,
          image: '../img/error.png',
          duration: 2000
        });
      } 
      success(res.data, param);
    },
    fail: function (e) {
      wx.showToast({
        title: '网络异常！err:authlogin',
        duration: 2000
      });
      if(null != failure)
      {
        failure(e,param);
      }
    },
  })
}

/**
 * 显示加载等待
 */
function showLoading() {
  if (wx.showLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.showLoading({
      title: "",
      mask: true
    });
  } else {
    // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
    wx.showToast({
      title: "",
      icon: 'loading',
      mask: true,
    });
  }
}
/**
 * 关闭加载等待
 */
function hideLoading() {
  if (wx.hideLoading) {
    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.hideLoading();
  } else {
    wx.hideToast();
  }
}

const Back = (v)=>{
  wx.navigateBack({delta:v});
}

const redirectTo = (url, that) => {
  if (that.globalData.bIsLink == true) {
  	console.log("Xgq redirectTo bIsLink false------")
    that.globalData.bIsLink = false,
      wx.redirectTo({
        url: url,
        complete: function () {
        	console.log("Xgq redirectTo complete bIsLink true------")
          that.globalData.bIsLink = true;
        }
      })
  } else {
    return;
  }
}
/**
 * 关闭所有页面 跳转到指定页面
 */
const reLaunch = (url,that)=>{
  if (that.globalData.bIsLink == true) {
  	console.log("Xgq reLaunch bIsLink false------")
    that.globalData.bIsLink = false,
      wx.reLaunch({
        url: url,
        complete: function () {
        	console.log("Xgq reLaunch complete bIsLink true------")
          that.globalData.bIsLink = true;
        }
      })
  } else {
    return;
  }
} 
const navigateTo = (url, that) => {
  // if(that.globaData.lasturl==url){
  //   Back(1);
  // }
  // that.globaData.lasturl = url;
  if (that.globalData.internal) {
    clearInterval(that.globalData.internal);
    that.globalData.internal = null;
  } 
  // showLoading();
  console.log("Xgq navigateTo:::url:" + url + " that.globaData.bIsLink:" + that.globalData.bIsLink);
  if (that.globalData.bIsLink == true) {
    console.log("Xgq navigateTo bIsLink false------")
    that.globalData.bIsLink = false,
      wx.navigateTo({
        url: url,
        complete: function () {
          that.globalData.bIsLink = true;
          console.log("Xgq navigateTo complete bIsLink true------")
        }
      })
  } else {
    return;
  }
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const formatPhoneNum = num =>{
  if(num == null || num.length < 11)
  {
    return num;
  }
  return num.substr(0, 3) + '*****' + num.substr(8);
}
/**
 * 获取用户信息
 */
function getUserInfo (app,callBackObj){
  if (app.globalData.userInfo) {
    if (null != callBackObj) {
      updateUserInfo(callBackObj,app);
    }
  } else if (callBackObj.data.canIUse) {
    // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
    // 所以此处加入 callback 以防止这种情况
    app.userInfoReadyCallback = res => {
      updateUserInfo(callBackObj,app);
    }
  } else {
    // 在没有 open-type=getUserInfo 版本的兼容处理
    wx.getUserInfo({
      success: res => {
        app.globalData.userInfo = res.userInfo
        updateUserInfo(callBackObj,app);
      }
    })
  }
}
/**
 * 更新用户信息
 */
function updateUserInfo(callBackObj,app)
{
  if(null != callBackObj)
  {
    callBackObj.setData({
      userInfo: app.globalData.userInfo,
      hasUserInfo: true
    })
    callBackObj.getUserInfoCallBack();
  }
}

function ShowModel(title, content, cancelText, confirmText, success, fail)
{
  wx.showModal({
    title: title,
    content: content,
    cancelText: cancelText,
    confirmText: confirmText,
    success: success,
    fail: fail
  });
}

function onShareApp()
{
  return {
    title: app.globalData.wxappName,
    path: '/pages/index/index',
    success: (res) => {
      console.log("转发成功", res);
    },
    fail: (res) => {
      console.log("转发失败", res);
    }
  }
}
/**
 * 从数据获取指定的属性
 */
function getObjByData(obj,id)
{
  if (null == obj || obj.hasOwnProperty(id))
  {
     return null;
  }
  return obj[id];
}

module.exports = {
  formatTime: formatTime, 
  request: request, 
  navigateTo: navigateTo, 
  redirectTo: redirectTo, 
  formatPhoneNum: formatPhoneNum, 
  getUserInfo: getUserInfo, 
  onShareApp: onShareApp, 
  getObjByData: getObjByData,
  getTokenHeadr: getTokenHeadr,
  reLaunch: reLaunch,
  Back:Back,
  hideLoading:hideLoading
}
