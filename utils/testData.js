function taskLineData()
{
  var tmpData = [
    {
      coursePlannedRouteId: 'A', //路线id
      routeName: "动物世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }
      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: 'B', //路线id
      routeName: "水世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: 'C', //路线id
      routeName: "天世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: ' D', //路线id
      routeName: "动物世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: 'E', //路线id
      routeName: "动物世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: 'F', //路线id
      routeName: "动物世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    },
    {
      coursePlannedRouteId: 'G', //路线id
      routeName: "动物世界", //路线名称
      courseApplyId: '1', //课程申请id
      courseApplyName: '课程名称', //课程申请的名字  
      usingCount: 20,  //正在被使用的人次
      plannedRouteTaskNodes: [
        {
          taskName: '打卡任务', //任务名称   
          taskImages: '/image/1.jpg', //任务图片   
          description: '由各种物质组成的巨型球状天体',  //描述   
          choiceQuestion: { //选择题
            questionTitle: '问题内容',
            choiceOptions: [
              {
                optionIndex: 0,
                optionItem: '题目1'
              },
              {
                optionIndex: 1,
                optionItem: '题目2'
              },
              {
                optionIndex: 2,
                optionItem: '题目3'
              }
            ], //选项集合
            fraction: '10',  //分值
            correctAnswerIndex: 1,//正确答案  从0开始
          }
        }

      ],  //节点信息集合 
    }
  ];
  return tmpData;
}
function plannedData(){
  return [ {
        planBeginTime:'2012/12/23',
        groupByStatusPlannedRouteRecords:[
          {
            recordStatus:"PLANNING",
            studentCoursePlannedRouteRecords:[
              {
                studentCoursePlannedRouteRecordId:'1' ,//学生课程记录id
                phoneNumber: "422545122", //属于那个手机号的
                fullName: '帅哥', //名字
                avatarUrl: "",//头像
                courseApplyId: "1", //申请课程id
                courseApplyName: "课程名称",  //申请课程名字
                customerUserOrdersCourseId: "1", //第二级订单id(课程订单id)
                coursePlannedRouteId: "A", //路线id
                coursePlannedRouteName: "动物路线",//路线的名字
                coursePlannedRoute: {
                  coursePlannedRouteId: 'G', //路线id
                  routeName: "动物世界", //路线名称
                  courseApplyId: '1', //课程申请id
                  courseApplyName: '课程名称', //课程申请的名字  
                  usingCount: 20,  //正在被使用的人次
                  plannedRouteTaskNodes: [
                    {
                      taskName: '打卡任务', //任务名称   
                      taskImages: '/image/1.jpg', //任务图片   
                      description: '由各种物质组成的巨型球状天体',  //描述   
                      choiceQuestion: { //选择题
                        questionTitle: '问题内容',
                        choiceOptions: [
                          {
                            optionIndex: 0,
                            optionItem: '题目1'
                          },
                          {
                            optionIndex: 1,
                            optionItem: '题目2'
                          },
                          {
                            optionIndex: 2,
                            optionItem: '题目3'
                          }
                        ], //选项集合
                        fraction: '10',  //分值
                        correctAnswerIndex: 1,//正确答案  从0开始
                      }
                    }

                  ],  //节点信息集合
                },//路线
                planBeginTime: "2012/5/4",          //计划上课时间
                recordStatus: "PLANNING",  //该记录的状态
                submitTime: "2012/20/5", //完成路线时间
                totalScore: "55", //总的得分
                rankRecord: {
                  studentCoursePlannedRouteRecordId: "1", //学生课程记录id
                  avatarUrl: "", //头像
                  phoneNumber: "5555",  //属于那个手机号的
                  fullName: "sdfsd",  //名字
                  totalScore: 60, //总得分
                  rank: 30 //排名
                } //排名情况
              }
            ]
          }
        ]
      }
    ];
}

function coursePlanData(){
  return [
    {
      planBeginTime: '2012/12/23',
      taskLine: [
        {
          recordStatus: 'PLANNING',
          studentCoursePlannedRouteRecordId: '1',
          courseApplyId: '1',
          courseApplyName: '课程名称',
          coursePlannedRouteId: 'A',
          selectLineName: 'A线路(动物路线)',
          rankRecord: {
            rank: 10
          },
        },
        {
          recordStatus: 'PLANNING',
          courseApplyName: '课程名称',
          coursePlannedRouteId: 'B',
          selectLineName: 'A线路(动物路线)',
          rankRecord: {
            rank: 10
          },
        }
      ]
    },
    {
      planBeginTime: '2012/11/23',
      taskLine: [
        {
          recordStatus: 'PLANNING',
          courseApplyName: '课程名称'
        }
      ]
    }
  ];
}

/**
 * 获取当前的打卡路线
 */
function curTaskLineData()
{
  return [
    {
        studentPlannedRouteTaskNodeRecordId:'1' , //学生课程路线节点记录Id
        studentCoursePlannedRouteRecordId: '1',     //属于那个路线记录的id
        //节点信息
        plannedRouteTaskNode: {
            taskName: '动物世界', //任务名称   
            taskImages: '', //任务图片   
          description: '由湖北省林业厅主管、花木盆景杂志社主办、武汉新兴绿色科技研究所联办的绿色科学与技术类学术期刊，是中国核心期刊（遴选）数据库、万方数字化期刊群、中文科技',  //描述   
            choiceQuestion: { //选择题
              questionTitle: '问题内容',
              choiceOptions: [
                {
                  optionIndex: 0,
                  optionItem: '题目1'
                },
                {
                  optionIndex: 1,
                  optionItem: '题目2'
                },
                {
                  optionIndex: 2,
                  optionItem: '题目3'
                }
              ], //选项集合
              fraction: '10',  //分值
              correctAnswerIndex: 1,//正确答案  从0开始
            }
          } ,
        submit: true ,  //是否完成节点
        submitTime: "2012/2/5", //完成节点时间
        nodeScore: 10  //节点得分
    },
    {
      studentPlannedRouteTaskNodeRecordId: '2', //学生课程路线节点记录Id
      studentCoursePlannedRouteRecordId: '1',     //属于那个路线记录的id
      //节点信息
      plannedRouteTaskNode: {
        taskName: '动物世界', //任务名称   
        taskImages: '', //任务图片   
        description: '邮发代号38-100，10元/期，年价120元)、《花木盆景盆景赏石版》(下半月刊，邮发代号38-101，22元/期',  //描述   
        choiceQuestion: { //选择题
          questionTitle: '问题内容',
          choiceOptions: [
            {
              optionIndex: 0,
              optionItem: '题目1'
            },
            {
              optionIndex: 1,
              optionItem: '题目2'
            },
            {
              optionIndex: 2,
              optionItem: '题目3'
            }
          ], //选项集合
          fraction: '10',  //分值
          correctAnswerIndex: 1,//正确答案  从0开始
        }
      },
      submit: false,  //是否完成节点
      submitTime: "2012/2/5", //完成节点时间
      nodeScore: 10  //节点得分
    },
    {
      studentPlannedRouteTaskNodeRecordId: '3', //学生课程路线节点记录Id
      studentCoursePlannedRouteRecordId: '1',     //属于那个路线记录的id
      //节点信息
      plannedRouteTaskNode: {
        taskName: '动物世界', //任务名称   
        taskImages: '', //任务图片   
        description: '传递市场信息、推动中国花卉盆景事业的发展为己任,深受全国各地读者朋友的喜爱,作为花木盆景类行业的举旗之刊,在业界一直享有盛誉,并具有深远影响。 杂志社每月编',  //描述   
        choiceQuestion: { //选择题
          questionTitle: '问题内容',
          choiceOptions: [
            {
              optionIndex: 0,
              optionItem: '题目1'
            },
            {
              optionIndex: 1,
              optionItem: '题目2'
            },
            {
              optionIndex: 2,
              optionItem: '题目3'
            }
          ], //选项集合
          fraction: '10',  //分值
          correctAnswerIndex: 1,//正确答案  从0开始
        }
      },
      submit: false,  //是否完成节点
      submitTime: "2012/2/5", //完成节点时间
      nodeScore: 10  //节点得分
    },
    {
      studentPlannedRouteTaskNodeRecordId: '4', //学生课程路线节点记录Id
      studentCoursePlannedRouteRecordId: '1',     //属于那个路线记录的id
      //节点信息
      plannedRouteTaskNode: {
        taskName: '动物世界', //任务名称   
        taskImages: '', //任务图片   
        description: '花木盆景杂志社是湖北省林业厅直属单位，创办于1984年4月，目前主办有《花木盆景》和《绿色科技》两种期刊',  //描述   
        choiceQuestion: { //选择题
          questionTitle: '问题内容',
          choiceOptions: [
            {
              optionIndex: 0,
              optionItem: '题目1'
            },
            {
              optionIndex: 1,
              optionItem: '题目2'
            },
            {
              optionIndex: 2,
              optionItem: '题目3'
            }
          ], //选项集合
          fraction: '10',  //分值
          correctAnswerIndex: 1,//正确答案  从0开始
        }
      },
      submit: false,  //是否完成节点
      submitTime: "2012/2/5", //完成节点时间
      nodeScore: 10  //节点得分
    }
  ];
}

/**
 * 打卡数据
 */
function taskData(){
  return {
    studentPlannedRouteTaskNodeRecordId: '4', //学生课程路线节点记录Id
      studentCoursePlannedRouteRecordId: '1',     //属于那个路线记录的id
        //节点信息
        plannedRouteTaskNode: {
      taskName: '动物世界', //任务名称   
        taskImages: '', //任务图片   
          description: '花木盆景杂志社是湖北省林业厅直属单位，创办于1984年4月，目前主办有《花木盆景》和《绿色科技》两种期刊',  //描述   
            choiceQuestion: { //选择题
        questionTitle: '问题内容',
          choiceOptions: [
            {
              optionIndex: 0,
              optionItem: '题目1'
            },
            {
              optionIndex: 1,
              optionItem: '题目2'
            },
            {
              optionIndex: 2,
              optionItem: '题目3'
            }
          ], //选项集合
            fraction: '10',  //分值
              correctAnswerIndex: 1,//正确答案  从0开始
        }
    },
    submit: false,  //是否完成节点
      submitTime: "2012/2/5", //完成节点时间
        nodeScore: 10  //节点得分
  }
}

function sortData(){
  var list = [
    {
        studentCoursePlannedRouteRecordId: '1', //学生课程记录id
        avatarUrl: '', //头像
        phoneNumber: '12324324234',  //属于那个手机号的
        fullName: '得分' , //名字
        totalScore: '100' ,//总得分
        rank: '55', //排名
    },
    {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '18650168782',  //属于那个手机号的
      fullName: '帅哥', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '232333',  //属于那个手机号的
      fullName: '史蒂芬', //名字
      totalScore: '100',//总得分
      rank: '5', //排名
    },
    {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '33435242',  //属于那个手机号的
      fullName: '递送', //名字
      totalScore: '100',//总得分
      rank: '54', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '43454646',  //属于那个手机号的
      fullName: '小妹', //名字
      totalScore: '100',//总得分
      rank: '23', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '233355',  //属于那个手机号的
      fullName: '大龄', //名字
      totalScore: '100',//总得分
      rank: '75', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '234455',  //属于那个手机号的
      fullName: '小强', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '23356',  //属于那个手机号的
      fullName: '潇湘', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '2342566',  //属于那个手机号的
      fullName: '小明', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '235456',  //属于那个手机号的
      fullName: '王麻子', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '23425436',  //属于那个手机号的
      fullName: '小二', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '234356',  //属于那个手机号的
      fullName: '李四', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }, {
      studentCoursePlannedRouteRecordId: '1', //学生课程记录id
      avatarUrl: '', //头像
      phoneNumber: '234546',  //属于那个手机号的
      fullName: '张三', //名字
      totalScore: '100',//总得分
      rank: '100', //排名
    }

  ];
  return list;
}

module.exports = {
  coursePlanData: coursePlanData,
  plannedData: plannedData,
  curTaskLineData: curTaskLineData,
  taskData: taskData,
  sortData: sortData
}