//app.js 
require("img.js");
App({
  data:{
    apiUrl: "https://witwin1.xiaomiqiu.com/", //调试运行地址
     apiUrl:"https://studytour.info:8085/",   //线上测试地址
     //apiUrl:https://studytour.info:8081,   //线上发布地址
  },
  onSaveSeesion : function(session){
    //wx.setStorageSync('session', session)
  },  
  onLaunch: function () {
    var that = this;
    // console.log("downstart......."+(new Date()).getTime());
    // wx.downloadFile({
    //   url: "https://cdn.studytour.info/f54fb8eb-48b5-45e4-85f2-6feb205c129f.jpg",//仅为示例，并非真实的资源
    //   success: function (res) {
    //     console.log("downend......." + (new Date()).getTime());
    //   } 
    // })
    // var session = wx.getStorageSync('session') || null;
    // if(null != session)
    // {
    //   that.globalData.sessionKey = session['sessionKey'];
    //   that.globalData.openID = session['openid'];
    // } 
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              that.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (that.userInfoReadyCallback) {
                that.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },

 
  globalData: {

    enterTaskNeedConfirm:false,//进入打卡 是否需要开启确认

    isLoading:false,//是否在加载数据

    code:null,       //登陆信息的code
    customerUserBean:null,//服务器上的用户信息
    userInfo: null,    //用户信息
    wxappName:'齐物志研学',   //小程序名字
    sessionKey:null,
    openID:null,
    unionID:null,

    phoneNumber:null,
    purePhoneNumber:null,
    countryCode:null, 

    bIsLink: true,          //用于页面跳转
    token:null,

    coursePlanData: null,//首页数据

    

    coursePlannedRouteId: 'A',                //当前选择的路线id
    studentCoursePlannedRouteRecordId: null, //学生课程记录id
    courseApplyId : null,
    routeName:null,               //用户当前打卡的路线名字

    customerUserOrdersCourseId:null,//用于加载排行榜

    //select task  课程对应的 所有路线列表
    coursePlannedRouteBeanList: null, //根据申请课程的id来获取相关的路线
    
    //scantask
    studentPlannedRouteTaskNodeRecordBeanList: null, //当前要打卡的路线路径
    studentPlannedRouteTaskNodeRecordBean:null,//当前选中打卡节点数据
    lastTaskCount:0,//当前路线还有多少节点没有打卡的数量
    taskScore:0,//答题得分

    courseApplyName:null,//用于查看排行榜的课程名称
    rankStudentCoursePlannedRouteRecordBeanList:null,//排名数据
 
    isSelectPage : false, //是否是选择页面
  }
})