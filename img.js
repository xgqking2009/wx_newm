        wx.getSystemInfo({
          success: function (res) {
            global.vw = res.screenWidth,global.vh = res.screenHeight;
            global.pr = res.pixelRatio;
          },
        })
        global.save = function(){
          global.ctx.save();
        }
        global.restore = function(){
          global.drawIOx = global.drawIOy = 0;
          global.ctx.restore();
        }
        global.mapidx = ['A', 'B', 'C', 'D', 'E', 'F'];
        global.clip = function(sx,sy,w,h){
          global.ctx.rect(sx*global.vw,sy*global.vh,w*global.vw,h*global.vh);
          global.ctx.clip();
        }
        global.circleClip = function(sx,sy,w){
          var ctx = global.ctx;
          ctx.save(); 
          ctx.beginPath(); 
          var r = w*global.vw; 
          ctx.arc(sx*global.vw+r, sy*global.vh+r, w*global.vw, 0, Math.PI * 2, false);
          ctx.clip();
        }
        global.turl = "https://cdn.studytour.info/";
        global.hmaps = {
          "BG_Sky_001.png":{},
          "BG_Sky_002.png": {},
          "BG_Tree_001.png": {},
          "BG_Tree_002.png": {},
          "BG_river_001.png": {},
        }
        global.btns = [];
        global.addBtns = function (allbtns){
          for (var akey in allbtns) {
            if (!global.btns[akey]) {
              global.btns[akey] = { bs: allbtns[akey], img: allbtns[akey].img, time: 0,key:akey};
              global.btns[akey].onEffect = (function () {
                this.time = global.scaleTTime;
              }).bind(global.btns[akey]);
            }
          }
        }
        global.drawBtn = function(btn){
          if (btn.time > 0) {
            btn.time -= 16.6;
            if (btn.time < 0) btn.time = 0;
          }
          global.imageScale = global.getImageScale(btn.time);
          global.drawImage(btn.img, btn.bs.x, btn.bs.y, btn.bs.w, btn.bs.h);
          global.imageScale = 0;
        }

        global.processBtn = function(bkey,bs,fn){
          global.canDrawImage = true;
          var map = {};map[bkey] = bs;
          global.addBtns(map);
          global.canDrawImage = true;
          var b = global.btns[bkey];
          b.bs = bs;
          if(b) {
            b.onClick = fn;
            global.drawBtn(global.btns[bkey]);
          }
        }
        global.tap = function(x,y){
            var btns = global.btns;
            for (var k in btns) {
              var bs = btns[k].bs;
              if (x >= bs.x && x < bs.x + bs.w && y >= bs.y && y < bs.y + bs.h) {
                btns[k].onClick();
                if(btns[k].onEffect) btns[k].onEffect();
              }
            }
        }
        global.loopBtns = function(){
          for (var k in global.btns) {
            var btn = global.btns[k];
            if (btn.time > 0) {
              btn.time -= 16.6;
              if (btn.time < 0) btn.time = 0;
            }
            global.imageScale = global.getImageScale(btn.time);
            global.drawImage(btn.img, btn.bs.x, btn.bs.y, btn.bs.w, btn.bs.h);
            global.imageScale = 0;
          }
        }
        var loadtask = [];
        for (var k in global.hmaps) {
          loadtask.push(k);
        } 
        function loadImage(loadtask) {
          if (loadtask.length == 0) return;
          wx.getImageInfo({
            src:  global.turl+loadtask[0],
            success: function (res) {
              global.hmaps[loadtask[0]] = {path:res.path};
              loadtask.splice(0, 1);
              loadImage(loadtask);
            },
            fail: function (res) {
              loadImage(loadtask);
            }
          });
        };
        // loadImage(loadtask);
        global.ctx = wx.createContext(); 
        global.flush = function(){
          wx.drawCanvas({ canvasId: 1, actions: global.ctx.getActions() });
        }
        global.canDrawImage = true;
        global.drawImage = function(img,px,py,m,ph){
          var bpos = img.indexOf("BG_");
          var hpos = img.indexOf("http");
          var oimg = ""+img;
          var wh = global.imginfo[img];
          if (bpos!=-1){
            img = img.slice(bpos,img.length);
            img = global.hmaps[img].path;
          }
          else if(hpos==0){
            var st = 1;
          }
          if(wh==undefined){
            if(!global.hmaps[img]||!global.hmaps[img].loaded){
              global.hmaps[img] = {loaded:true};
              wx.getImageInfo({
                src: img,
                success: function (res) {
                  global.imginfo[img] = [res.width / 2, res.height / 2];
                  global.hmaps[img].path = res.path;
                },
                fail: function (res) {
                }
              });
              return;
            }
            if(global.hmaps[img].path){
              img = global.hmaps[img].path;
            }else{
              return;
            }
          }

          if(global.hmaps[img]){
            img = global.hmaps[img].path;
          }
          if(!img) return; var ctx = global.ctx;
          if(ph!=undefined){
            if(global.canDrawImage)
            {
              if (global.imageScale==0) ctx.drawImage(img, px * global.vw, py * global.vh,
               m * global.vw, ph * global.vh);
              else{
                var s = global.imageScale;
                ctx.drawImage(img,px * global.vw - m * global.vw *s*0.5+global.drawIOx, 
                  py * global.vh - ph*global.vh *s*0.5, m * global.vw*(1+s), ph * global.vh*(1+s));
              } 
            } 
            return { img: oimg, x: px, y: py, w: m, h: ph};
          }else
          if(px==-1){ 
            if(m==undefined)    px = 0.5-wh[0]/global.vw/2;
            else {px = m.x+ m.w/2 - wh[0]/global.vw/2; py = m.y+py;}
          }     
          var rpx = px*global.vw,rpy = py*global.vh;
          if(px==-2) { 
            if (global.canDrawImage) ctx.drawImage(img,0,0,global.vw,global.vh);
            return { img: oimg, x: 0, y: 0, w: 1, h: 1 };
          }  
          else if (px == -3){
            if (global.canDrawImage) ctx.drawImage(img, 0, 0, global.vw, wh[1]);
            return { img: oimg, x: 0, y: 0, w: 1, h: wh[1]/global.h };
          } 
          else {
            if (global.canDrawImage) ctx.drawImage(img,rpx,rpy,wh[0],wh[1]); 
            return { img: oimg,x:px,y:py,w:wh[0]/global.vw,h:wh[1]/global.vh};
          }
        };
        global.getImageScale = function(t){
          return t > global.scaleTTime/2 ? (global.scaleTTime - t) / 800 :  t / 800;
        };
        global.scaleTTime = 166;
        global.drawText = function(text,px,py,fsize,color)
        {
          var ctx = global.ctx;
          var rpx = (px+global.drawIOx) * global.vw, rpy = (py + global.drawIOy) * global.vh ; ctx.setFillStyle(color);
          ctx.setFontSize(fsize);ctx.fillText(text,rpx,rpy);
        };

        global.drawTextInRect = function(text,px,py,pw,ph,maxline,fsize,color)
        {
          var ctx = global.ctx,canvasWidth = pw * global.vw;
          var rpx = px * global.vw + global.drawIOx, rpy = py * global.vh + global.drawIOy; ctx.setFillStyle(color);
          ctx.setFontSize(fsize);
          let lposx = px *global.vw,w2w = fsize*2;
          let str = text;
          let lastSubStrIndex = 0; //每次开始截取的字符串的索引
          let line = 0,lineWidth = fsize,lposy = py * global.vh;
          for (let i = 0; i < str.length; i++) {
              lineWidth += ctx.measureText(str[i]).width;
              if (lineWidth >canvasWidth) {
                  if(line == 0){
                    lposx = px * global.vw + w2w;
                    
                  }else{
                    lposx = px * global.vw;
                  }
                var sustr = str.substring(lastSubStrIndex, i);
                ctx.fillText(sustr, lposx, lposy); //绘制截取部分
                lposy+=(fsize+2);lastSubStrIndex = i;line++;lineWidth = 0;
                if(line>=maxline) return;
                  // titleHeight += 22;
              }
              if (i == str.length - 1) { //绘制剩余部分
                  ctx.fillText(str.substring(lastSubStrIndex, i + 1), lposx, lposy);
              }
          }
        }

        global.drawImagePx = function(ctx,img,px,py,pw,ph){
          if(pw==undefined){
            var wh = global.imginfo[img];
            ctx.drawImage(img,px,py,wh[0],wh[1])
          }
          else{
            ctx.drawImage(img,px,py,pw,ph)
          }
        };

        global.CLONE = function (obj) {
          var newobj = obj.constructor === Array ? [] : {};
          if (typeof obj !== 'object') {
            return;
          }
          for (var i in obj) {
            newobj[i] = obj[i]&&typeof obj[i] === 'object' ?
              global.CLONE(obj[i]) : obj[i];
          }
          return newobj
        } 

        global.Clamp = function(v,min,max){return v<min?min:v>max?max:v;}

        global.list = function(x,y,w,h,cw,ch,dir){
          this.x = x;this.y = y;this.w = w;this.h = h;this.cw = cw;this.ch = ch;
          this.lx = x;this.ly = y;
          this.dir = dir;
          this.touch = false;
          this.dx = this.dy = 0;
        }
        var proto = global.list.prototype;
        proto.touchStart = function(x,y){
          var pwx = global.smx + x,pwy = global.smy + y;
          this.dx = this.dy = this.dminx = this.dmaxx = 0;
          this.px = this.x;this.py = this.y;
          if(pwx>=this.lx&&pwx<=this.lx+this.w&&pwy>=this.ly&&pwy<this.ly+this.h){
            this.touch = true; this.touchonce = true;
            // console.log("Xgq touchonce true");
          }
        }
        proto.touchMove = function(x,y){
          if(this.touch == false) return;
          var pwx = global.smx + x, pwy = global.smy + y;
          if(this.touchonce==true){
            this.ox = pwx; this.oy = pwy; this.touchonce = false;
            this.px = this.x;this.py = this.y;
            var dlcx = this.w - this.cw; 
            // console.log("Xgq this.w:"+this.w+" this.cw:"+this.cw+" dlcx:"+dlcx);
            if(dlcx>0) dlcx = 0;
            var cminx = this.lx + dlcx;
            this.dminx = cminx - this.x;
            this.dmaxx = this.lx - this.x;
            // console.log("Xgq this.dminx:"+this.dminx+" this.dmaxx:"+this.dmaxx);
            return;
          } 
          this.dx = pwx - this.ox;this.dy = pwy - this.oy;
          if(this.dir == 'V'){  this.dx = 0;}
          else if(this.dir == 'H') {this.dy = 0;}
          if(this.dir == 'H'){
            this.dx = global.Clamp(this.dx,this.dminx,this.dmaxx);
          }
        }
        global.drawIOx = 0;
        global.drawIOy = 0;
        proto.pwx = function(){
          return this.x+global.smx + this.dx;
        }
        proto.pwy = function () {
          return this.y+global.smy + this.dy;
        }
        proto.touchEnd = function(x,y){
          if(this.touch == false) return;
          this.x = this.px + this.dx;
          this.dx = 0;
          this.touch = false;
        }
        proto.clip = function(){
          global.ctx.save();
          var pwx = this.lx + global.smx,pwy = this.ly + global.smy;
          global.clip(pwx, pwy, this.w, this.h); 
        } 
        global.smx = 0;
        global.smy = 0;
        global.s  = global.vw/global.vh;
        global.imginfo = {
    "../img/BG_river_001.png":[360,882],
"../img/BG_Sky_001.png":[360,640],
"../img/BG_Sky_002.png":[360,640],
"../img/BG_Tree_001.png":[360,640], 
"../img/BG_Tree_002.png":[279,146],
"../img/Botton_001_Normal.png":[164,34],
"../img/Botton_002_Normal.png":[164,34],
"../img/Botton_003_Normal.png":[114,35],
"../img/Botton_004_Normal.png":[114,35],
"../img/Botton_005_Normal.png":[71,73],
"../img/Botton_006_Normal.png":[71,73],
"../img/Botton_007_Normal.png":[67,69],
"../img/Botton_008_Normal.png":[82,83],
"../img/Botton_009_Normal.png":[39,40],
"../img/Botton_010_Normal.png":[68,19],
"../img/Botton_011_Normal.png":[58,19],
"../img/Botton_012_Normal.png":[58,19],
"../img/Botton_013_Normal.png":[49,51],
"../img/Botton_014_Normal.png":[186,51],
"../img/Botton_015_Normal.png":[188,38],
"../img/Botton_016_Normal.png":[188,38],
"../img/Botton_017_Normal.png":[187,38],
"../img/Botton_018_Normal.png":[118,39],
"../img/Botton_019_Normal.png":[118,39],
"../img/Botton_020_Normal.png":[118,39],
"../img/Botton_A_Normal.png":[59,61],
"../img/Botton_A_Selected.png":[59,61],
"../img/Botton_B_Normal.png":[59,61],
"../img/Botton_B_Selected.png":[59,61],
"../img/Botton_C_Normal.png":[59,61],
"../img/Botton_C_Selected.png":[59,61],
"../img/Botton_D_Normal.png":[59,61],
"../img/Botton_D_Selected.png":[59,61],
"../img/Botton_E_Normal.png":[59,61],
"../img/Botton_E_Selected.png":[59,61],
"../img/Botton_F_Normal.png":[59,61],
"../img/Botton_F_Selected.png":[59,61],
"../img/Botton_G_Normal.png":[59,61],
"../img/Botton_G_Selected.png":[59,61],
"../img/Botton_H_Normal.png":[59,61],
"../img/Botton_H_Selected.png":[59,61],
"../img/Botton_I_Normal.png":[59,61],
"../img/Botton_I_Selected.png":[59,61],
"../img/Botton_J_Normal.png":[59,61],
"../img/Botton_J_Selected.png":[59,61],
"../img/Botton_K_Normal.png":[59,61],
"../img/Botton_K_Selected.png":[59,61],
"../img/Botton_L_Normal.png":[59,61],
"../img/Botton_L_Selected.png":[59,61],
"../img/Botton_M_Normal.png":[59,61],
"../img/Botton_M_Selected.png":[59,61],
"../img/Botton_N_Normal.png":[59,61],
"../img/Botton_N_Selected.png":[59,61],
"../img/Botton_Return_Normal.png":[65,43],
"../img/Bubble_001.png":[74,38],
"../img/Bubble_010.png":[81,59],
"../img/Bubble_011.png":[81,59],
"../img/Correct_001.png":[32,30],
"../img/Inputbox_001.png":[236,39],
"../img/Inputbox_002.png":[144,39],
"../img/Line_001.png":[185,7],
"../img/Line_002.png":[271,2],
"../img/Logo_001.png":[67,72],
"../img/Num_01.png":[25,20],
"../img/Num_02.png":[25,20],
"../img/Num_03.png":[25,20],
"../img/Num_04.png":[25,20],
"../img/Num_05.png":[25,20],
"../img/Num_06.png":[25,20],
"../img/Num_07.png":[25,20],
"../img/Num_08.png":[25,20],
"../img/Num_09.png":[25,20],
"../img/Num_10.png":[25,20],
"../img/Num_11.png":[25,20],
"../img/Num_12.png":[25,20],
"../img/Num_13.png":[25,20],
"../img/Num_14.png":[25,20],
"../img/Num_15.png":[25,20],
"../img/Num_16.png":[25,20],
"../img/Num_17.png":[25,20],
"../img/Num_18.png":[25,20],
"../img/Num_19.png":[25,20],
"../img/Num_20.png":[25,20],
"../img/Num_21.png":[25,20],
"../img/Num_22.png":[25,20],
"../img/Num_23.png":[25,20],
"../img/Num_24.png":[25,20],
"../img/Num_25.png":[25,20],
"../img/Num_26.png":[25,20],
"../img/Num_27.png":[25,20],
"../img/Num_28.png":[25,20],
"../img/Num_29.png":[25,20],
"../img/Num_30.png":[25,20],
"../img/Panel_001.png":[274,161],
"../img/Panel_002.png":[312,312],
"../img/Panel_003.png":[312,288],
"../img/Panel_004.png":[202,120],
"../img/Panel_005.png":[312,558],
"../img/Panel_008.png":[218,33],
"../img/Panel_009.png":[227,112],
"../img/Panel_010.png":[184,57],
"../img/Panel_011.png":[220,46],
"../img/Panel_012.png":[133,83],
"../img/Panel_013.png":[155,83],
"../img/Panel_014.png":[120,122],
"../img/Panel_015.png":[37,37],
"../img/Panel_016.png":[21,21],
"../img/Role_001.png":[108,220],
"../img/Star_001.png":[37,38],
"../img/Star_002.png":[40,40],
"../img/Star_003.png":[48,50],
"../img/Star_006.png":[259,259],
"../img/Textrue_001.png":[222,106],
"../img/Textrue_002.png":[222,106],
"../img/Textrue_003.png":[222,106],
}

var proto;
global.sprite = function(spriteData){
  this.spriteData = spriteData;
}
proto = global.sprite.prototype;
proto.draw = function(){var s = this.spriteData;
  var ctx = this.spriteData.ctx;
  var pw = s.pw,ph = s.ph,pw = s.pw,ph = s.ph,cm = s.cm;
  
}
global.button = function(spriteData,onClick){
  this.spriteData = spriteData;
  this.onClick = onClick;
}